package core;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;

/**
 * Created by devuser on 5/11/17.
 */
public interface IDICOMItem {
    Association getAS();

    PresentationContext getPC();

    Dimse getDimse();

    Attributes getReq();

    Attributes getDataAtt();

    PDVInputStream getDataPDV();

    String getFromAE();

    String getInstanceUID();

    String getPatientUID();

    String getStudyUID();

    String[] getSeriesUID();

    String getImageID();

    String getSOPClass();

    String getTagValue(String tagName);

    String setTagValue(String tagName, String value);

    void returnResult(int status);
}
