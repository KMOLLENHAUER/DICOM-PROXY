package core.hosts;

/**
 * Created by devuser on 5/18/17.
 */
public interface IHost {
    boolean isReachable();

    String getName();

    String getAe();

    String getIp();

    int getPort();

    String getDescription();
}
