package core.hosts;


import core.utils.ConfigurationManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by devuser on 5/18/17.
 */
public class HostManager {
    private final static Logger LOG = LoggerFactory.getLogger(HostManager.class);
    private final static ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(HostManager.class);
    private static HostManager INSTANCE = null;
    private Map<String, IHost> hostsMap = new HashMap<>();

    private HostManager() {
    }

    public static HostManager getINSTANCE() {
        // if no instance exists.
        if (INSTANCE == null) {
            // new one
            INSTANCE = new HostManager();
            // try to read the configuration file and load it.
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(CONFIGURATION_MANAGER.getProperty("hosts.path")));
                JSONArray conf = (JSONArray) obj;

                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    String description = (String) jsonObj.get("description");
                    String ae = (String) jsonObj.get("ae");
                    String ip = (String) jsonObj.get("ip");
                    String port = Long.toString((Long) jsonObj.get("port"));
                    INSTANCE.add(name, description, ae, ip, port);
                });
            } catch (IOException e) {
                LOG.warn("Actions configuration file not found. New configuration will be create. {}", e.toString());
            } catch (ParseException e) {
                LOG.error("Impossible to parse the configuration file. New configuration will be create. {}", e.toString());
            }

            // Save current conf
            INSTANCE.saveConf();
        }
        // return host manager
        return INSTANCE;
    }

    public void saveConf() {
        // The conf file in JSON
        JSONArray hostConf = new JSONArray();
        // each Action
        hostsMap.forEach((k, v) -> {
            JSONObject host = new JSONObject();
            host.put("name", k);
            host.put("description", v.getDescription());
            host.put("ae", v.getAe());
            host.put("ip", v.getIp());
            host.put("port", v.getPort());
            hostConf.add(host);
        });
        // Write it!
        try (FileWriter file = new FileWriter(CONFIGURATION_MANAGER.getProperty("hosts.path"))) {
            file.write(hostConf.toJSONString());
        } catch (IOException e) {
            LOG.error("Can't save the host configuration. {}", e.toString());
        }
    }

    public void add(String name, String description, String ae, String ip, String port) throws IllegalArgumentException {
        if (name == null || description == null || ae == null || ip == null || port == null)
            throw new IllegalArgumentException("Arguments missing.");
        if (hostsMap.containsKey(name))
            throw new IllegalArgumentException("Host name already taken.");
        try {
            Integer.getInteger(port);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Wrong port number format.");
        }
        try {
            Inet4Address.getByName(ip);
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException("Wrong ip address format.");
        }
        hostsMap.put(name, new Host(name, ae, description, ip, Integer.parseInt(port)));
    }

    public void deleteByName(String id) {
        hostsMap.remove(id);
        LOG.info("Host : {} removed.", id);
    }

    public IHost getByName(String name) {
        return hostsMap.get(name);
    }

    public String getAll() {
        JSONArray result = new JSONArray();
        hostsMap.forEach((k, v) -> {
            JSONObject host = new JSONObject();
            host.put("name", k);
            host.put("description", v.getDescription());
            host.put("ae", v.getAe());
            host.put("ip", v.getIp());
            host.put("port", v.getPort());
            result.add(host);
        });
        return result.toString();
    }
}

