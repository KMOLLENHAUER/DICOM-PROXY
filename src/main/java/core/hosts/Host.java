package core.hosts;

import core.utils.ConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Objects;


/**
 * Created by devuser on 5/18/17.
 */
public class Host implements IHost {
    private final static Logger LOG = LoggerFactory.getLogger(Host.class);
    private final static ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(Host.class);

    private String name;
    private String description;
    private String ae;
    private String ip;
    private int port;

    public Host(String name, String description, String ae, String ip, int port) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(description);
        Objects.requireNonNull(ae);
        Objects.requireNonNull(ip);
        Objects.requireNonNull(port);

        this.name = name;
        this.description = description;
        this.ae = ae;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public boolean isReachable() {
        try {
            InetAddress i = InetAddress.getByName(this.ip);
            return i.isReachable(Integer.parseInt(CONFIGURATION_MANAGER.getProperty("network.pingtimeout", "2000")));
        } catch (IOException e) {
            LOG.warn("{} at address {} is not reachable.\n Error : {}", this.ae, this.ip, e.getMessage());
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAe() {
        return ae;
    }

    public void setAe(String ae) {
        this.ae = ae;
    }

    @Override
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        Objects.requireNonNull(description);
        this.description = description;
    }

    @Override
    public String toString() {
        return this.ae + "@" + this.ip + ":" + this.port;
    }
}
