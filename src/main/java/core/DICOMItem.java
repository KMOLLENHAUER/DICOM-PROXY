package core;

import core.dicom_in.DicomItemCallbacks;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.util.TagUtils;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Objects;

import static org.dcm4che3.util.TagUtils.forName;

/**
 * Created by devuser on 4/25/17.
 */
public class DICOMItem implements IDICOMItem {
    private final Association as;
    private final PresentationContext pc;
    private final Dimse dimse;
    private final Attributes rq;
    private final PDVInputStream dataIS;
    private final Attributes dataA;
    private final String fromAE;
    private final String transferSyntax;
    private final String patientUID;
    private final String studyID;
    private final String series;
    private final String imageID;
    private final String SOPClass;
    private final DicomItemCallbacks cb;

    public DICOMItem(Association as, PresentationContext pc, Dimse dimse, Attributes rq, PDVInputStream dataIS) {
        throw new NotImplementedException();
        /*Objects.requireNonNull(as);
        Objects.requireNonNull(pc);
        Objects.requireNonNull(dimse);
        Objects.requireNonNull(rq);

        this.as = as;
        this.pc = pc;
        this.dimse = dimse;
        this.rq = rq;
        this.dataIS = dataIS;
        this.dataA = null;
        this.fromAE = as.getCallingAET();
        this.instanceUID = "";
        this.patient = "";
        this.studyID = "";
        this.series = "";
        this.SOP = "";*/
    }

    public DICOMItem(Association as, PresentationContext pc, Dimse dimse, Attributes rq, Attributes dataA, DicomItemCallbacks cb) {
        Objects.requireNonNull(as);
        Objects.requireNonNull(pc);
        Objects.requireNonNull(dimse);
        Objects.requireNonNull(rq);

        this.as = as;
        this.pc = pc;
        this.dimse = dimse;
        this.rq = rq;
        this.dataIS = null;
        this.dataA = dataA;
        this.fromAE = as.getCallingAET();
        this.transferSyntax = dataA.getString(Tag.TransferSyntaxUID);
        this.patientUID = dataA.getString(Tag.PatientID);
        this.studyID = dataA.getString(Tag.StudyInstanceUID);
        this.series = dataA.getString(Tag.SeriesInstanceUID);
        this.SOPClass = UID.nameOf(dataA.getString(Tag.SOPClassUID));
        this.imageID = dataA.getString(Tag.SOPInstanceUID);
        this.cb = cb;
    }

    public static void main(String[] args) {
        System.out.println(forName("PrivateInformation"));
    }

    @Override
    public Association getAS() {
        return as;
    }

    @Override
    public PresentationContext getPC() {
        return pc;
    }

    @Override
    public Dimse getDimse() {
        return dimse;
    }

    @Override
    public Attributes getReq() {
        return rq;
    }

    @Override
    public Attributes getDataAtt() {
        return dataA;
    }

    @Override
    public PDVInputStream getDataPDV() {
        return dataIS;
    }

    @Override
    public String getFromAE() {
        return this.fromAE;
    }

    @Override
    public String getInstanceUID() {
        return this.transferSyntax;
    }

    @Override
    public String getPatientUID() {
        return this.patientUID;
    }

    @Override
    public String getStudyUID() {
        return this.studyID;
    }

    @Override
    public String[] getSeriesUID() {
        return new String[]{this.series};
    }

    @Override
    public String getImageID() {
        return this.imageID;
    }

    @Override
    public String getSOPClass() {
        return this.SOPClass;
    }

    @Override
    public String getTagValue(String tagName) {
        int tagID = TagUtils.forName(tagName);
        return dataA.getString(TagUtils.forName(tagName));
    }

    @Override
    public String setTagValue(String tagName, String value) {
        int tagID = TagUtils.forName(tagName);
        VR vr = dataA.getVR(tagID);
        dataA.setString(tagID, vr, value);
        return "OK";
    }

    @Override
    public void returnResult(int status) {
        this.cb.onResult(status);
    }
}

