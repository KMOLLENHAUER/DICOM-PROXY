package core.dicom_assoc;

import core.hosts.IHost;
import org.dcm4che3.net.Association;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by devuser on 7/18/17.
 */
public class AssocManager {
    private static final Logger LOG = LoggerFactory.getLogger(AssocManager.class);
    private static AssocManager INSTANCE = null;
    private final Map<Association, AssocSession> assocMap = new HashMap<>();

    private AssocManager() {

    }

    public static AssocManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AssocManager();
        }
        return INSTANCE;
    }

    public void createSession(Association receiverAssoc) {
        synchronized (assocMap) {
            this.assocMap.put(receiverAssoc, new AssocSession());
            LOG.info("Session created from AET {}", receiverAssoc.getCallingAET());
        }
    }

    public boolean existSession(Association receiverAssoc) {
        synchronized (assocMap) {
            return this.assocMap.containsKey(receiverAssoc);
        }
    }

    public boolean isBindSession(Association receiverAssoc, String hostAE) {
        synchronized (assocMap) {
            if (this.assocMap.get(receiverAssoc).getEmitterAssoc(hostAE) != null) {
                if (this.assocMap.get(receiverAssoc).getEmitterAssoc(hostAE) != null) return true;
            }
            return false;
        }
    }

    public void bindEmitter(Association receiverAssoc, Association emitterAssoc, IHost h) {
        synchronized (assocMap) {
            this.assocMap.get(receiverAssoc).addEmitterAssoc(emitterAssoc, h.getAe());
            LOG.info("Session bind between rcvAET {} to emitAET {}", receiverAssoc.getCallingAET(), emitterAssoc.getCalledAET());
        }
    }

    public AssocSession getSession(Association receiverAssoc) {
        synchronized (assocMap) {
            return this.assocMap.get(receiverAssoc);

        }
    }

    public void removeSession(Association receiverAssoc) {
        synchronized (assocMap) {
            this.assocMap.remove(receiverAssoc);
            LOG.info("Session rcvAET {} removed", receiverAssoc.getCallingAET());
        }
    }

    public void addReceiveMsg(Association receiverAssoc) {
        //TODO
    }

    public void addEmitterMsg(Association receiverAssoc) {
        //TODO
    }

    public void setReceiverFinished(Association receiverAssoc) {
        synchronized (assocMap) {
            this.assocMap.get(receiverAssoc).setFinished(true);
            LOG.info("Session rcvAET {} ends on the receiver side", receiverAssoc.getCallingAET());
        }
    }

    public boolean canCloseSession(Association receiverAssociation) {
        //TODO
        return false;
    }
}
