package core.dicom_assoc;

import core.dicom_out.scu_services.IScuServices;
import org.dcm4che3.net.Association;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by devuser on 7/18/17.
 */
public class AssocSession {
    private Map<String, Association> emitterAssocs = new HashMap<>();
    private Map<Integer, Boolean> ackMsg = new HashMap<>();
    private boolean isFinished = false;
    private IScuServices scuServices = null;

    public void addEmitterAssoc(Association assoc, String hostAE) {
        this.emitterAssocs.put(hostAE, assoc);
    }

    public Association getEmitterAssoc(String hostAE) {
        return this.emitterAssocs.get(hostAE);
    }

    public void setFinished(boolean finished) {
        this.isFinished = finished;
    }
}
