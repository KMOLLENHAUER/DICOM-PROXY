package core.triggers;


import core.IDICOMItem;

/**
 * Created by devuser on 4/25/17.
 */
public interface ITrigger {
    void set(String... params);

    boolean isTrig(IDICOMItem dcm);

    String getName();

    String getType();

    String[] getParams();
}
