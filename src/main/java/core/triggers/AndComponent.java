package core.triggers;

import core.IDICOMItem;
import gui.JSONable;

import java.util.Objects;

/**
 * Created by devuser on 6/21/17.
 */
public class AndComponent implements ITrigger, JSONable {
    // To translate trigger name in trigger instance
    private static final TriggersManager TM = TriggersManager.getINSTANCE();
    private static final String type = "and";

    private String name;
    private ITrigger comp1;
    private ITrigger comp2;


    public AndComponent(String name) {
        Objects.requireNonNull(name);

        this.name = name;

        this.comp1 = null;
        this.comp2 = null;
    }

    public AndComponent(String name, String params[]) throws IllegalArgumentException {
        if (params.length != 2) throw new IllegalArgumentException("Too few arguments.");

        this.name = name;

        ITrigger temp1 = TM.getByName(params[0]);
        ITrigger temp2 = TM.getByName(params[1]);

        if (temp1 == null || temp2 == null)
            throw new IllegalArgumentException("One or more parameters are null. May Trigger manager can't found them : " + params[0] + "; " + params[1]);

        set(temp1, temp2);
    }

    public static void main(String[] args) throws IllegalAccessException {
        System.out.println(new AndComponent(null, null));
    }

    public void set(ITrigger t1, ITrigger t2) {
        this.comp1 = t1;
        this.comp2 = t2;
    }

    @Override
    public void set(String params[]) {
        if (params.length != 2) throw new IllegalArgumentException("Too few arguments.");

        ITrigger temp1 = TM.getByName(params[0]);
        ITrigger temp2 = TM.getByName(params[1]);

        this.set(temp1, temp2);
    }

    @Override
    public boolean isTrig(IDICOMItem dcm) {
        if (comp1 == null || comp2 == null) throw new IllegalStateException("Components are not initialized.");
        return this.comp1.isTrig(dcm) && this.comp2.isTrig(dcm);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String[] getParams() {
        return new String[]{this.comp1.getName(), this.comp2.getName()};
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
