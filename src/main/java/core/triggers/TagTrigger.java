package core.triggers;

import core.IDICOMItem;
import gui.JSONable;

import java.util.Objects;

/**
 * Created by devuser on 7/6/17.
 */
public class TagTrigger implements ITrigger, JSONable {
    private static final String type = "tag";
    private final String name;
    private String tagname;
    private String tagvalue;

    public TagTrigger(String name) {
        Objects.requireNonNull(name);
        this.name = name;

        this.tagname = null;
        this.tagvalue = null;
    }

    public TagTrigger(String name, String params[]) {
        if (params.length != 2) throw new IllegalArgumentException("Too few arguments.");

        this.name = name;

        this.tagname = params[0];
        this.tagvalue = params[1];
    }

    @Override
    public void set(String params[]) {
        if (params.length != 2) throw new IllegalArgumentException("Too few arguments.");

        this.set(params[0], params[1]);
    }

    @Override
    public boolean isTrig(IDICOMItem dcm) {
        return dcm.getTagValue(tagname).equals(tagvalue);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String[] getParams() {
        return new String[]{this.tagname, this.tagvalue};
    }

    public void set(String tagname, String value) {
        this.tagname = tagname;
        this.tagvalue = value;
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
