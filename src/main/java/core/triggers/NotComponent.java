package core.triggers;

import core.IDICOMItem;
import gui.JSONable;

import java.util.Objects;

/**
 * Created by devuser on 6/21/17.
 */
public class NotComponent implements ITrigger, JSONable {
    // To translate trigger name in trigger instance
    private static final TriggersManager TM = TriggersManager.getINSTANCE();
    private static final String type = "not";

    private String name;
    private ITrigger comp;

    public NotComponent(String name) {
        Objects.requireNonNull(name);
        this.name = name;

        this.comp = null;
    }

    public NotComponent(String name, String params[]) throws IllegalArgumentException {
        if (params.length != 1) throw new IllegalArgumentException("Too few arguments.");

        this.name = name;

        ITrigger temp = TM.getByName(params[0]);

        if (temp == null)
            throw new IllegalArgumentException("Parameter is null. May Trigger manager can't found them : " + params[0]);

        set(temp);
    }

    public NotComponent(ITrigger comp) {
        Objects.requireNonNull(comp);
        this.comp = comp;
    }

    public void set(ITrigger t) {
        Objects.requireNonNull(t);

        this.comp = t;
    }

    @Override
    public void set(String params[]) {
        if (params.length != 1) throw new IllegalArgumentException("Too few arguments.");

        ITrigger temp = TM.getByName(params[0]);

        this.set(temp);
    }

    @Override
    public boolean isTrig(IDICOMItem dcm) {
        if (comp == null) throw new IllegalStateException("Component is not initialized.");
        return !this.comp.isTrig(dcm);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String[] getParams() {
        return new String[]{this.comp.getName()};
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
