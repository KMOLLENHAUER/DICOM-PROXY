package core.triggers;

import core.IDICOMItem;
import gui.JSONable;

import java.util.Objects;

/**
 * Created by devuser on 4/25/17.
 */
public class DefaultTrigger implements ITrigger, JSONable {
    @Override
    public void set(String... params) {
        // Nothing
    }

    /**
     * The default trigger's isTrig is always true.
     * It's used during development or for debugging.
     *
     * @param dcm a DICOMItem instance
     * @return true if the DICOMItem corresponds or false otherwise
     */
    @Override
    public boolean isTrig(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);
        return true;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public String[] getParams() {
        return new String[0];
    }

    @Override
    public String toString() {
        return "for any";
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
