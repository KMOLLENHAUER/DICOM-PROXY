package core.triggers;

import core.utils.ConfigurationManager;
import gui.JSONable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by devuser on 5/11/17.
 */
public class TriggersManager implements JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(TriggersManager.class);

    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(TriggersManager.class);

    private static TriggersManager INSTANCE;

    private final Map<String, ITrigger> triggerMap = new ConcurrentHashMap<>();

    public static TriggersManager getINSTANCE() {
        // if no instance exists.
        if (INSTANCE == null) {
            // new one
            INSTANCE = new TriggersManager();
            // try to read the configuration file and load it.
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(CONFIGURATION_MANAGER.getProperty("triggers.path")));
                JSONArray conf = (JSONArray) obj;

                //FIRST ONLY CREATE ALL OBJECTS.
                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    String type = (String) jsonObj.get("type");
                    INSTANCE.createTriggers(type, name);
                });

                //THEN, WE CAN LINK THEM
                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    JSONArray params = (JSONArray) jsonObj.get("params");
                    INSTANCE.linkTriggers(name, Arrays.stream(params.toArray()).toArray(String[]::new));
                });
            } catch (IOException e) {
                LOG.warn("Triggers configuration file not found. New configuration will be create. {}", e.toString());
            } catch (ParseException e) {
                LOG.error("Impossible to parse the configuration file. New configuration will be create. {}", e.toString());
            }

            // Save current conf
            INSTANCE.saveConf();
        }
        // return trigger manager
        return INSTANCE;
    }

    public static void main(String[] args) {
        TriggersManager TM = TriggersManager.getINSTANCE();

        /*TM.add("tag", "tag1", "tagname1", "tagvalue1");
        TM.add("tag", "tag2", "tagname2", "tagvalue2");
        TM.add("and", "tag1&tag2", "tag1", "tag1");*/

        ITrigger t1 = TM.getByName("tag1");
        ITrigger t2 = TM.getByName("tag2");
        ITrigger t3 = TM.getByName("tag1&tag2");

        System.out.println(t1.getName());
        System.out.println(t2.getName());
        System.out.println(t3.getName());

        System.out.println(TM.getAll());

        TM.saveConf();
    }

    private void createTriggers(String triggertype, String name) {
        if (triggertype.equals("tag")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new TagTrigger(name));
            LOG.info("Trigger {} type TAG created", name);
            return;
        }

        if (triggertype.equals("and")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new AndComponent(name));
            LOG.info("Trigger {} type AND created", name);
            return;
        }

        if (triggertype.equals("or")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new OrComponent(name));
            LOG.info("Trigger {} type OR created", name);
            return;
        }

        if (triggertype.equals("not")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new NotComponent(name));
            LOG.info("Trigger {} type NOT created", name);
            return;
        }
    }

    private void linkTriggers(String name, String[] params) {
        if (triggerMap.containsKey(name)) {
            ITrigger trig = triggerMap.get(name);
            trig.set(params);
            triggerMap.replace(name, trig);
        } else {
            throw new IllegalStateException("Can't link trigger " + name + " to " + params);
        }
    }

    public void add(String triggertype, String name, String... params) throws IllegalArgumentException {
        if (triggertype.equals("tag")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new TagTrigger(name, params));
            LOG.info("Trigger {} type tag added", name);
            return;
        }

        if (triggertype.equals("and")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new AndComponent(name, params));
            LOG.info("Trigger {} type tag added", name);
            return;
        }

        if (triggertype.equals("or")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new OrComponent(name, params));
            LOG.info("Trigger {} type OR created", name);
            return;
        }

        if (triggertype.equals("not")) {
            if (triggerMap.containsKey(name)) throw new IllegalArgumentException("Trigger name is already taken.");
            triggerMap.put(name, new NotComponent(name, params));
            LOG.info("Trigger {} type NOT created", name);
            return;
        }

        LOG.warn("Trigger : {} , no corresponding type : {}", name, triggertype);
        throw new IllegalArgumentException("unknown trigger type.");
    }

    public void deleteByName(String id) {
        triggerMap.remove(id);
        LOG.info("Trigger : {} removed.", id);
    }

    public ITrigger getByName(String name) {
        return triggerMap.get(name);
    }

    public void saveConf() {
        // The conf file in JSON
        JSONArray triggersConf = new JSONArray();
        // each trigger
        triggerMap.forEach((k, v) -> {
            JSONObject trig = new JSONObject();
            trig.put("name", k);
            trig.put("type", v.getType());
            JSONArray params = new JSONArray();
            for (String s : v.getParams()) {
                params.add(s);
            }
            trig.put("params", params);
            triggersConf.add(trig);
        });
        // Write it!
        try (FileWriter file = new FileWriter(CONFIGURATION_MANAGER.getProperty("triggers.path"))) {
            file.write(triggersConf.toJSONString());
        } catch (IOException e) {
            LOG.error("Can't save the trigger configuration. {}", e.toString());
        }
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        JSONArray result = new JSONArray();

        triggerMap.forEach((k, v) -> {
            JSONObject trig = new JSONObject();
            JSONArray params = new JSONArray();
            for (String s : v.getParams()) {
                params.add(s);
            }
            trig.put("name", k);
            trig.put("type", v.getType());
            trig.put("params", params);
            result.add(trig);
        });

        return result.toString();
    }

}
