package core.proxy;


import core.IDICOMItem;
import core.actions.IAction;
import core.datas.IWriteDataConnectors;
import core.datas.WriteMongoDBConnector;
import core.datas.logs.IData;
import core.router.Router;
import core.triggers.ITrigger;
import gui.JSONable;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Created by devuser on 4/25/17.
 */
public class Rule implements IRule, JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(Rule.class);
    private static final IWriteDataConnectors DB_CONNECTOR = WriteMongoDBConnector.getInstance();
    private final String name;
    private final String description;
    private ITrigger trigger;
    private IAction action;
    private Router router = Router.getInstance();

    /*
    public Rule(){
        this.triggers.add(new DefaultTrigger());
        this.actions.add(new DefaultAction());
        this.name = "Default rule";
        this.description = "This is doing nothing and is applicable on every DICOMItem.";
    }*/

    /**
     * Rule constructor with parameter to avoid the use of default trigger and default parameter.
     *
     * @param t List of ITriggers implementations
     * @param a List of IActions implementations
     */
    public Rule(String name, String description, ITrigger t, IAction a) {
        Objects.requireNonNull(t);
        Objects.requireNonNull(a);
        Objects.requireNonNull(name);
        Objects.requireNonNull(description);

        this.name = name;
        this.description = description;
        this.trigger = t;
        this.action = a;



        /*//For each new trigger, if this trigger isn't already in the list, add it.
        t.forEach(trig->{
            if(!this.triggers.contains(trig)){
                this.triggers.add(trig);
            }else{
                LOG.warn("Trying to add an already existing trigger to the rule {} : {}",this.toString(), trig.toString());
            }
        });

        //For each new action, if this action isn't already in the list, add it.
        a.forEach(act->{
            if(!this.actions.contains(act)){
                this.actions.add(act);
            }else{
                LOG.warn("Trying to add an already existing action to the rule {} : {}",this.toString(), act.toString());
            }
        });*/
    }

    /**
     * Return true if at least one trigger is applicable to the given DICOMItem.
     *
     * @param dcm a DICOMItem
     * @return true if applicable false otherwise
     */
    @Override
    public boolean isApplicable(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);

        /*boolean isApp = false;
        for(ITrigger t : triggers){
            if(t.isTrig(dcm)){
                return true;
            }
        }*/
        return this.trigger.isTrig(dcm);
    }

    /**
     * Apply the each action of this rule to the DICOMItem.
     *
     * @param dcm DICOMItem to be modified
     * @return return the modified DICOMItem
     */
    @Override
    public IDICOMItem apply(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);

        LOG.info("Rule {} is applied on {}.", this.name, dcm);

        IDICOMItem tmp = dcm;
        /*for(IAction a : actions){
            tmp = a.apply(tmp);
        }*/
        //TODO save the original dcmItem to keep a backup of modification.
        return this.action.apply(dcm);
    }

    /**
     * Transform to IData. To be able to log it in DB
     *
     * @return
     */
    public IData toIData() {
        return new IData() {
            @Override
            public Document toDocument() {
                Document doc = new Document();
                doc.append("rule_name", name);
                doc.append("rule_description", description);
                doc.append("rule_trigger", trigger);
                doc.append("rule_action", action);
                return doc;
            }
        };
    }

    /**
     * Return the rule name to string format
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Return the rule description to string format
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getTrigger() {
        return trigger.getName();
    }

    @Override
    public String getAction() {
        return action.getName();
    }

    /**
     * Return trigger name
     *
     * @return
     */
    public String getTriggerName() {
        return this.trigger.getName();
    }

    /**
     * Return action name
     *
     * @return
     */
    public String getActionName() {
        return this.action.getName();
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
