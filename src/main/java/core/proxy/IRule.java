package core.proxy;

import core.IDICOMItem;

/**
 * Created by devuser on 7/12/17.
 */
public interface IRule {
    boolean isApplicable(IDICOMItem dcm);

    IDICOMItem apply(IDICOMItem dcm);

    String getName();

    String getDescription();

    String getTrigger();

    String getAction();
}
