package core.proxy;


import core.IDICOMItem;
import core.actions.ActionsManager;
import core.actions.IAction;
import core.datas.IWriteDataConnectors;
import core.datas.WriteMongoDBConnector;
import core.datas.logs.PerfData;
import core.datas.logs.ProxingData;
import core.datas.requests.MongoWriteDataRequest;
import core.router.Router;
import core.triggers.ITrigger;
import core.triggers.TriggersManager;
import core.utils.ConfigurationManager;
import gui.JSONable;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * Created by devuser on 4/25/17.
 */
public class RulesTable implements JSONable {
    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(RulesTable.class);
    // Conf manager
    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(RulesTable.class);
    // Database connection
    private static final IWriteDataConnectors DB_CONN = WriteMongoDBConnector.getInstance();
    private static final int QUEUE_SIZE = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("proxy.queuesize", "10"));
    private static final int NB_THREADS = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("proxy.nbthread", "10"));
    // Own instance
    private static RulesTable INSTANCE = null;
    // Triggers manager
    private final TriggersManager TRIGGERS_MANAGER = TriggersManager.getINSTANCE();
    // Actions manager
    private final ActionsManager ACTIONS_MANAGER = ActionsManager.getINSTANCE();
    private final Map<String, IRule> rulesMap = new ConcurrentHashMap<>();
    private final BlockingQueue<IDICOMItem> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    private Router router = Router.getInstance();
    private ExecutorService process = Executors.newFixedThreadPool(NB_THREADS);

    private RulesTable() {
    }

    public static RulesTable getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RulesTable();

            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(CONFIGURATION_MANAGER.getProperty("proxy.path")));
                JSONArray conf = (JSONArray) obj;

                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    String trigger = (String) jsonObj.get("trigger");
                    String action = (String) jsonObj.get("action");
                    String descript = (String) jsonObj.get("description");
                    INSTANCE.add(name, descript, trigger, action);
                });
            } catch (IOException e) {
                LOG.warn("Rules configuration file not found. New configuration will be create. {}", e.toString());
            } catch (ParseException e) {
                LOG.error("Impossible to parse the configuration file. New configuration will be create. {}", e.toString());
            }

            // Save current conf
            INSTANCE.saveConf();
            LOG.info("Rules in RulesTable have been successfully load/reload.");
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        RulesTable rt = new RulesTable();

        rt.add("rule1", "tag1&tag2", "testtag1", "no description");

        rt.saveConf();
    }

    /**
     * Test the applicability of each rule to the DICOMItem and apply them if possible.
     *
     * @param dcm a DICOMItem
     */
    private void checkIt(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);

        final IDICOMItem[] modifDCM = new IDICOMItem[1];

        LOG.info("DICOMItem submit to the rules table.");
        rulesMap.forEach((k, v) -> {
            if (v.isApplicable(dcm)) {
                long start = System.currentTimeMillis();
                LOG.info("Rule {} is applicable", v.getName());
                modifDCM[0] = v.apply(dcm);
                LOG.info("FIRING PROXY RULE => " + v.toString());
                DB_CONN.set(new Document().append("collection", "proxying"),
                        new ProxingData(new Date(), dcm.getFromAE(), dcm.getInstanceUID(), dcm.getStudyUID(),
                                dcm.getSeriesUID(), dcm.getPatientUID(), dcm.getImageID(),
                                dcm.getSOPClass(), v.getName()),
                        new MongoWriteDataRequest(),
                        t -> {
                            if (t != null) {
                                LOG.error("{} {}", t.getMessage(), t.toString());
                            }
                        });
                long time = System.currentTimeMillis() - start;
                DB_CONN.set(new Document().append("collection", "perf"), new PerfData("proxy", v.getName(), time), new MongoWriteDataRequest(),
                        t -> {
                            if (t != null) {
                                LOG.error("{} {}", t.getMessage(), t.toString());
                            }
                        });
            }
        });

        if (modifDCM[0] != null) {
            LOG.info("A DICOMItem add in the router queue.");
            router.addInQueue(modifDCM[0]);
        } else {
            router.addInQueue(dcm);
            LOG.info("A DICOMItem not proxy applicable!");
        }
    }

    /**
     * Add an DICOMItem to the DICOMItem queue and try to launch a rules worker on it.
     *
     * @param dI a DICOMItem
     */
    public void addInQueue(IDICOMItem dI) {
        Objects.requireNonNull(dI);

        try {
            this.queue.put(dI);
        } catch (InterruptedException e) {
            LOG.error("Interrupted during trying to put {} in the queue.", dI.toString());
        }
        this.run();
    }

    /**
     * Launch a worker on the queue. If no rules worker is available, a new one is created.
     */
    private void run() {
        process.submit(() -> {
            try {
                LOG.info("New Rule worker is created.");
                IDICOMItem dcm = queue.take();
                LOG.info("DICOMItem takes from the proxy rules table queue.");
                checkIt(dcm);
            } catch (InterruptedException ie) {
                LOG.error("Rule worker was interrupted while taking DICOMItem.");
                return;
            }
            LOG.info("Rule worker finished his job.");
        });
    }

    /**
     * Try to gently stop all rules workers.
     */
    public void stop() {
        try {
            process.shutdown();
            //wait for 5 seconds before kill workers.
            process.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.info("Interrupted during workers shutting down.");
        } finally {
            //KILL THEM ALL!
            if (!process.isTerminated()) {
                LOG.warn("Workers was brutally killed.");
            }
            process.shutdownNow();
            LOG.info("All workers are stopped.");
        }
    }

    /**
     * Set the router instance.
     *
     * @param r a Router instance.
     */
    public void setRouter(Router r) {
        Objects.requireNonNull(r);

        this.router = r;
    }

    public void add(String name, String description, String triggername, String actionname) throws IllegalArgumentException {
        Objects.requireNonNull(name);
        Objects.requireNonNull(description);
        Objects.requireNonNull(triggername);
        Objects.requireNonNull(actionname);

        if (rulesMap.containsKey(name)) throw new IllegalArgumentException("Rule already exist.");

        ITrigger trig = TRIGGERS_MANAGER.getByName(triggername);
        IAction act = ACTIONS_MANAGER.getByName(actionname);

        if (trig == null) throw new IllegalArgumentException("Trigger " + triggername + " is not exiting.");
        if (act == null) throw new IllegalArgumentException("Action " + actionname + " is not exiting.");

        rulesMap.put(name, new Rule(name, description, trig, act));
        LOG.info("New rule adds => name : {} triggername : {} actionname : {} description : {}."
                , name, triggername, actionname, description);
    }

    public void deleteByName(String id) {
        rulesMap.remove(id);
        LOG.info("Rule : {} removed.", id);
    }

    public void saveConf() {
        // The conf file in JSON
        JSONArray proxyConf = new JSONArray();
        // each Action
        rulesMap.forEach((k, v) -> {
            JSONObject rul = new JSONObject();
            rul.put("name", v.getName());
            rul.put("description", v.getDescription());
            rul.put("trigger", v.getTrigger());
            rul.put("action", v.getAction());
            proxyConf.add(rul);
        });

        // Write it!
        try (FileWriter file = new FileWriter(CONFIGURATION_MANAGER.getProperty("proxy.path"))) {
            file.write(proxyConf.toJSONString());
        } catch (IOException e) {
            LOG.error("Can't save the proxy configuration. {}", e.toString());
        }
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        JSONArray result = new JSONArray();

        rulesMap.forEach((k, v) -> {
            JSONObject trig = new JSONObject();
            JSONArray params = new JSONArray();
            trig.put("name", v.getName());
            trig.put("description", v.getDescription());
            trig.put("trigger", v.getTrigger());
            trig.put("action", v.getAction());
            result.add(trig);
        });

        return result.toString();
    }
}
