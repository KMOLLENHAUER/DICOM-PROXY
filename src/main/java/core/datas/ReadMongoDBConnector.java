package core.datas;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import core.datas.requests.DatabaseReaderCallback;
import core.datas.requests.IReadRequests;
import core.datas.requests.MongoReadDataRequest;
import core.utils.ConfigurationManager;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by devuser on 6/6/17.
 */
public class ReadMongoDBConnector implements IReadDataConnectors {

    private static final Logger LOG = LoggerFactory.getLogger(ReadMongoDBConnector.class);

    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager
            .getInstance(ReadMongoDBConnector.class);

    private static final ReadMongoDBConnector CONNECTOR_INSTANCE = new ReadMongoDBConnector();

    private final Map<String, MongoCollection<Document>> mongoCollections = new HashMap<>();

    private Integer limit;

    private ReadMongoDBConnector() {
        try {
            final MongoClient mongoClient = MongoClients.create(CONFIGURATION_MANAGER.getProperty("database.host"));
            final MongoDatabase mongoDatabase = mongoClient.getDatabase(CONFIGURATION_MANAGER
                    .getProperty("database.datasource"));
            for (String s : CONFIGURATION_MANAGER.getProperty("database.collections").split(";")) {
                this.mongoCollections.put(s, mongoDatabase.getCollection(s));
            }
        } catch (IllegalArgumentException e) {
            LOG.error(e.getMessage());
            throw new IllegalStateException(e);
        }
        int tmp = 5000;
        try {
            tmp = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("database.limit"));
        } catch (IllegalArgumentException e) {
            LOG.warn("Use default database.limit");
        }
        this.limit = tmp;
        LOG.info("ReadMongoDBConnector has been instantiate");
    }

    public static IReadDataConnectors getInstance() {
        return CONNECTOR_INSTANCE;
    }

    public static void main(String[] args) throws InterruptedException {

        ReadMongoDBConnector dbCon = ReadMongoDBConnector.CONNECTOR_INSTANCE;

        IReadRequests req = new MongoReadDataRequest();

        DatabaseReaderCallback dbR = (t, result) -> System.out.println("Request result : " + result);

        Document doc = new Document();
        doc.append("collection", "routing");

        dbCon.get(doc, req, dbR, 10);


        Thread.sleep(10000);

        System.out.println("MAIN ends.");


    }

    @Override
    public void get(Document doc, IReadRequests request, DatabaseReaderCallback callback, int limit) {
        if (this.mongoCollections.containsKey(doc.getString("collection"))) {
            MongoCollection<Document> coll = this.mongoCollections.get(doc.getString("collection"));
            request.apply(coll, doc, callback, limit);
        } else {
            LOG.error("Trying to obtain unknown collection {}", doc.getString("collection"));
            callback.onResult(new IllegalArgumentException("Unknown collection " + doc.getString("collection")),
                    "unknown collection " + doc.getString("collection"));
            return;
        }

    }

    @Override
    public void get(Document doc, IReadRequests request, DatabaseReaderCallback callback) {
        get(doc, request, callback, this.limit);
    }
}
