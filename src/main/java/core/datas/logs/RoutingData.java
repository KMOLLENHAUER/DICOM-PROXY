package core.datas.logs;

import org.bson.Document;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by devuser on 6/7/17.
 */
public class RoutingData implements IData {

    private final Date date;
    private final String from;
    private final String to;
    private final String instanceUID;
    private final String studyUID;
    private final String[] seriesUID;
    private final String patientUID;
    private final String imageID;
    private final String SOPUID;
    private final String outSyntax;

    public RoutingData(Date date, String from, String to, String instanceUID, String studyUID, String[] seriesUID, String patientUID, String imageID, String sopuid, String outSyntax) {
        this.date = date;
        this.from = from;
        this.to = to;
        this.instanceUID = instanceUID;
        this.studyUID = studyUID;
        this.seriesUID = seriesUID;
        this.patientUID = patientUID;
        this.imageID = imageID;
        this.SOPUID = sopuid;
        this.outSyntax = outSyntax;
    }

    @Override
    public Document toDocument() {
        Document doc = new Document();
        doc.append("date", this.date.toString());
        doc.append("from", this.from);
        doc.append("to", this.to);
        doc.append("instanceUID", this.instanceUID);
        doc.append("studyUID", this.studyUID);
        doc.append("seriesUID", Arrays.stream(this.seriesUID).collect(Collectors.joining(",", "[", "]")));
        doc.append("patientUID", this.patientUID);
        doc.append("imageID", this.imageID);
        doc.append("SOPUID", this.SOPUID);
        doc.append("outSyntax", this.outSyntax);
        return doc;
    }
}
