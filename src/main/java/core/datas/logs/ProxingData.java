package core.datas.logs;

import org.bson.Document;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by devuser on 6/13/17.
 */
public class ProxingData implements IData {

    private final Date date;
    private final String from;
    private final String instanceUID;
    private final String studyUID;
    private final String[] seriesUID;
    private final String patientUID;
    private final String imageID;
    private final String SOPUID;
    private final String proxy_action;

    public ProxingData(Date date, String from, String instanceUID, String studyUID, String[] seriesUID, String patientUID, String imageID, String sopuid, String proxy_action) {
        this.date = date;
        this.from = from;
        this.instanceUID = instanceUID;
        this.studyUID = studyUID;
        this.seriesUID = seriesUID;
        this.patientUID = patientUID;
        this.imageID = imageID;
        this.SOPUID = sopuid;
        this.proxy_action = proxy_action;
    }

    @Override
    public Document toDocument() {
        Document doc = new Document();
        doc.append("date", this.date.toString());
        doc.append("from", this.from);
        doc.append("studyUID", this.studyUID);
        doc.append("seriesUID", Arrays.stream(this.seriesUID).collect(Collectors.joining(",", "[", "]")));
        doc.append("patientUID", this.patientUID);
        doc.append("strudyID", this.studyUID);
        doc.append("imageID", this.imageID);
        doc.append("SOPUID", this.SOPUID);
        doc.append("action", this.proxy_action);
        return doc;
    }
}
