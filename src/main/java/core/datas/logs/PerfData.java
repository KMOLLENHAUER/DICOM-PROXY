package core.datas.logs;

import org.bson.Document;

/**
 * Created by devuser on 6/29/17.
 */
public class PerfData implements IData {

    private final String type;
    private final String name;
    private final Long time;

    public PerfData(String type, String name, Long time) {
        this.type = type;
        this.name = name;
        this.time = time;
    }

    @Override
    public Document toDocument() {
        Document doc = new Document();
        doc.append("type", this.type);
        doc.append("name", this.name);
        doc.append("time", this.time);
        return doc;
    }
}
