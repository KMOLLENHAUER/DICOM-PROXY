package core.datas.logs;

import org.bson.Document;

/**
 * Created by devuser on 6/7/17.
 */
@FunctionalInterface
public interface IData {
    Document toDocument();
}
