package core.datas;


import core.datas.requests.DatabaseReaderCallback;
import core.datas.requests.IReadRequests;
import org.bson.Document;

/**
 * Created by devuser on 6/6/17.
 */
public interface IReadDataConnectors {
    void get(Document collection, IReadRequests request, DatabaseReaderCallback callback, int limit);

    void get(Document collection, IReadRequests request, DatabaseReaderCallback callback);
}
