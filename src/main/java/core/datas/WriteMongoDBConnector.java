package core.datas;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import core.datas.logs.IData;
import core.datas.logs.ProxingData;
import core.datas.logs.RoutingData;
import core.datas.requests.DatabaseWriterCallback;
import core.datas.requests.IWriteRequests;
import core.datas.requests.MongoWriteDataRequest;
import core.utils.ConfigurationManager;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by devuser on 6/7/17.
 */
public class WriteMongoDBConnector implements IWriteDataConnectors {

    private static final Logger LOG = LoggerFactory.getLogger(WriteMongoDBConnector.class);

    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(WriteMongoDBConnector.class);

    private static WriteMongoDBConnector CONNECTOR_INSTANCE = null;

    private final Map<String, MongoCollection<Document>> mongoCollections = new HashMap<>();

    private WriteMongoDBConnector() {
        try {
            final MongoClient mongoClient = MongoClients.create(CONFIGURATION_MANAGER.getProperty("database.host"));
            final MongoDatabase mongoDatabase = mongoClient.getDatabase(CONFIGURATION_MANAGER.getProperty("database.datasource"));
            for (String s : CONFIGURATION_MANAGER.getProperty("database.collections").split(";")) {
                this.mongoCollections.put(s, mongoDatabase.getCollection(s));
            }
        } catch (IllegalArgumentException e) {
            LOG.error(e.getMessage());
            throw new IllegalStateException(e);
        }
        LOG.info("WriteMongoDBConnector has been instantiate");
    }

    public static IWriteDataConnectors getInstance() {
        if (CONNECTOR_INSTANCE == null) {
            CONNECTOR_INSTANCE = new WriteMongoDBConnector();
        }
        return CONNECTOR_INSTANCE;
    }

    public static void main(String[] args) throws InterruptedException {
        IWriteDataConnectors dataCon = WriteMongoDBConnector.getInstance();

        if (dataCon == null) System.out.println("DataCon is null");

        /*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));*/

        // ROUTING

        String[] series1 = {"serie1", "serie2", "serie3"};
        String[] series2 = {"serie4"};

        IData routData1 = new RoutingData(new Date(), "from1", "to2", "instanceUID", "study1", series1, "patient1", "imageID", "CT", "LE");
        IData routData2 = new RoutingData(new Date(), "from1", "to3", "instanceUID", "study2", series2, "patient1", "imageID", "XP", "LE");

        IWriteRequests writeReq = new MongoWriteDataRequest();
        DatabaseWriterCallback callback = (Throwable t) -> LOG.info("request done!");
        // routing doc
        Document routingDoc = new Document();
        routingDoc.append("collection", "routing");
        // proxying doc
        Document proxyingDoc = new Document();
        proxyingDoc.append("collection", "proxying");

        for (int i = 10; i > 0; i--) {
            dataCon.set(routingDoc, routData1, writeReq, callback);
            dataCon.set(routingDoc, routData2, writeReq, callback);
        }

        // PROXYING

        IData proxData = new ProxingData(new Date(), "from1", "instanceUID", "study1", series1, "patient1", "imageID", "CT", "doing things...");

        for (int i = 10; i > 0; i--) {
            dataCon.set(proxyingDoc, proxData, writeReq, callback);
        }

        Thread.sleep(10000);

        System.out.println("MAIN ends.");
    }

    @Override
    public void set(Document doc, IData data, IWriteRequests request, DatabaseWriterCallback callback) {
        if (this.mongoCollections.containsKey(doc.getString("collection"))) {
            MongoCollection<Document> coll = this.mongoCollections.get(doc.getString("collection"));
            request.apply(coll, data, callback);
        } else {
            LOG.error("Trying to obtain unknown collection {}", doc.getString("collection"));
            callback.onResult(new IllegalArgumentException("Unknown collection " + doc.getString("collection")));
            return;
        }
    }
}
