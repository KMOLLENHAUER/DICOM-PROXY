package core.datas.requests;

/**
 * Created by devuser on 6/7/17.
 */

public interface DatabaseWriterCallback {
    void onResult(Throwable t);
}
