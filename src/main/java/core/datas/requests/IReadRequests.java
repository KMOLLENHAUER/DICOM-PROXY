package core.datas.requests;


import com.mongodb.async.client.MongoCollection;
import org.bson.Document;

/**
 * Created by devuser on 6/6/17.
 */
public interface IReadRequests {
    String toString();

    void apply(MongoCollection<Document> col, Document doc, DatabaseReaderCallback callback, Integer limit);
}
