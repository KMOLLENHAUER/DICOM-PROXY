package core.datas.requests;


import com.mongodb.async.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by devuser on 6/7/17.
 */
public class MongoReadDataRequest implements IReadRequests {

    @Override
    public void apply(MongoCollection<Document> col, Document doc, DatabaseReaderCallback callback, Integer limit) {
        col.find().sort(new Document().append("id", -1)).limit(limit).into(new ArrayList<>(), (result, t) -> callback.onResult(
                t, "[" + result.stream().map(Document::toJson).collect(Collectors.joining(", ")) + "]"));
    }
}
