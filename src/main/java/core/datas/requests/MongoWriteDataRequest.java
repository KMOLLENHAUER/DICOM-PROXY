package core.datas.requests;


import com.mongodb.async.client.MongoCollection;
import core.datas.logs.IData;
import org.bson.Document;


/**
 * Created by devuser on 6/7/17.
 */
public class MongoWriteDataRequest implements IWriteRequests {
    @Override
    public void apply(MongoCollection<Document> col, IData data, DatabaseWriterCallback callback) {
        col.insertOne(data.toDocument(), (result, t) -> callback.onResult(t));
    }
}
