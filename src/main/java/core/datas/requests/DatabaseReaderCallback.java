package core.datas.requests;

/**
 * Created by devuser on 6/6/17.
 */
public interface DatabaseReaderCallback {
    void onResult(Throwable t, String result);
}
