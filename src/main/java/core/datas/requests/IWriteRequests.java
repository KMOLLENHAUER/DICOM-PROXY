package core.datas.requests;


import com.mongodb.async.client.MongoCollection;
import core.datas.logs.IData;
import org.bson.Document;

/**
 * Created by devuser on 6/7/17.
 */
public interface IWriteRequests {
    void apply(MongoCollection<Document> coll, IData data, DatabaseWriterCallback callback);
}
