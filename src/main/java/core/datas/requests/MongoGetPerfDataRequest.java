package core.datas.requests;

import com.mongodb.async.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by devuser on 6/30/17.
 */
public class MongoGetPerfDataRequest implements IReadRequests {
    @Override
    public void apply(MongoCollection<Document> col, Document doc, DatabaseReaderCallback callback, Integer limit) {
        col.find(new Document().append("type", doc.get("type"))).sort(new Document().append("_id", -1)).limit(limit).into(new ArrayList<>(), (result, t) -> callback.onResult(
                t, "[" + result.stream().map(Document::toJson).collect(Collectors.joining(", ")) + "]"));
    }
}
