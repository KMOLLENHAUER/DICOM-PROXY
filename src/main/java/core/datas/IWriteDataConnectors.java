package core.datas;


import core.datas.logs.IData;
import core.datas.requests.DatabaseWriterCallback;
import core.datas.requests.IWriteRequests;
import org.bson.Document;


/**
 * Created by devuser on 6/7/17.
 */
public interface IWriteDataConnectors {
    void set(Document doc, IData data, IWriteRequests request, DatabaseWriterCallback callback);
}
