package core.dicom_out;


import core.IDICOMItem;
import core.dicom_assoc.AssocManager;
import core.hosts.IHost;
import core.utils.ConfigurationManager;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by devuser on 5/3/17.
 */
public class Emitter {
    private final static Logger LOG = LoggerFactory.getLogger(Emitter.class);
    private final static ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(Emitter.class);
    private final static int MAX_RETRY = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("network.maxretry", "3"));
    private final static int TIME_BEFORE_RETRY = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("network.timebeforeretry", "100"));
    private final AssocManager ASSOC_MANAGER = AssocManager.getInstance();
    private int nbRetry = MAX_RETRY;
    private ApplicationEntity fromae = new ApplicationEntity("VB10"); //CONFIGURATION_MANAGER.getProperty("dicom.aetitle");
    private Device dev = new Device("VB10"); //CONFIGURATION_MANAGER.getProperty("dicom.name");

    public Emitter() {
        Connection conn = new Connection();
        //conn.setBindAddress(CONFIGURATION_MANAGER.getProperty("network.hostname"));

        this.dev.addConnection(conn);
        this.dev.addApplicationEntity(this.fromae);
        this.fromae.addConnection(conn);
        initWorker();
    }

    private PresentationContext setPresentationContext(IDICOMItem dcm) {
        return new PresentationContext(1, dcm.getDataAtt().getString(Tag.SOPClassUID),
                UID.ExplicitVRLittleEndian);
    }

    private String setTransfertSyntax(String cuid, Association receiverAssoc) {
        Set<String> tss = receiverAssoc.getTransferSyntaxesFor(cuid);
        if (tss.contains(UID.ExplicitVRLittleEndian))
            return UID.ExplicitVRLittleEndian;

        return UID.ImplicitVRLittleEndian;
    }

    public Association open(IHost h, IDICOMItem dcm) throws InterruptedException, GeneralSecurityException, IncompatibleConnectionException, IOException {
        AAssociateRQ rq = new AAssociateRQ();
        rq.setMaxOpsInvoked(0);
        rq.setMaxOpsPerformed(0);
        rq.addPresentationContext(setPresentationContext(dcm));
        rq.setCalledAET(h.getAe());

        Connection remote = new Connection();
        remote.setHostname(h.getIp());
        remote.setPort(h.getPort());

        return this.fromae.connect(remote, rq);
    }

    public void close(Association as) throws IOException, InterruptedException {
        if (as != null) {
            if (as.isReadyForDataTransfer())
                as.release();
            as.waitForSocketClose();
        }
    }

    public void Emit(IHost h, IDICOMItem dcmI) {
        try {
            // If Session not existing, create it.
            if (!ASSOC_MANAGER.isBindSession(dcmI.getAS(), h.getAe())) {
                LOG.info("New emitter association with host {}", h.getAe());
                Association emitterAssoc = open(h, dcmI);
                ASSOC_MANAGER.bindEmitter(dcmI.getAS(), emitterAssoc, h);
            }
        } catch (InterruptedException e) {
            LOG.warn("InterruptedException during emitting of a STORE_RQ : {}", e.getMessage());
            //this.retry(h, dcmI);
        } catch (GeneralSecurityException e) {
            LOG.warn("GeneralSecurityException during emitting of a STORE_RQ : {}", e.getMessage());
            //this.retry(h, dcmI);
        } catch (IncompatibleConnectionException e) {
            LOG.warn("IncompatibleConnectionException during emitting of a STORE_RQ : {}", e.getMessage());
            //this.retry(h, dcmI);
        } catch (IOException e) {
            LOG.warn("IOException during emitting of a STORE_RQ : {}", e.toString());
            //this.retry(h, dcmI);
        }


        if (dcmI.getDimse().equals(Dimse.C_STORE_RQ)) {
            LOG.info("Will emit a C_STORE_RQ.");

            try {
                Association as = ASSOC_MANAGER.getSession(dcmI.getAS()).getEmitterAssoc(h.getAe());
                as.cstore(dcmI.getDataAtt().getString(Tag.SOPClassUID),
                        dcmI.getDataAtt().getString(Tag.SOPInstanceUID),
                        1,
                        new DataWriterAdapter(dcmI.getDataAtt()),
                        setTransfertSyntax(dcmI.getDataAtt().getString(Tag.SOPClassUID), dcmI.getAS()),
                        createDimseRSPHandler(dcmI.getAS(), dcmI)
                );
                ASSOC_MANAGER.addEmitterMsg(dcmI.getAS());
                LOG.info("Store send to host {}", h.getName());

                if (ASSOC_MANAGER.canCloseSession(dcmI.getAS())) {
                    close(as);
                    ASSOC_MANAGER.removeSession(dcmI.getAS());
                    LOG.info("Emitter association close with host {}", h.getName());
                }
            } catch (InterruptedException e) {
                LOG.warn("InterruptedException during emitting of a STORE_RQ : {}", e.getMessage());
                //this.retry(h, dcmI);
            } catch (IOException e) {
                LOG.warn("IOException during emitting of a STORE_RQ : {}", e.toString());
                //this.retry(h, dcmI);
            }
        }
        if (dcmI.getDimse().equals(Dimse.C_FIND_RQ)) {

        }
        if (dcmI.getDimse().equals(Dimse.C_GET_RQ)) {

        }
        if (dcmI.getDimse().equals(Dimse.C_MOVE_RQ)) {

        }
    }

    private DimseRSPHandler createDimseRSPHandler(Association as, IDICOMItem dcm) {

        return new DimseRSPHandler(as.nextMessageID()) {

            @Override
            public void onDimseRSP(Association as, Attributes cmd,
                                   Attributes data) {
                super.onDimseRSP(as, cmd, data);
                onCRSP(cmd, dcm);
            }
        };
    }

    private void onCRSP(Attributes cmd, IDICOMItem dcm) {
        int status = cmd.getInt(Tag.Status, -1);
        switch (status) {
            case Status.Success:
                LOG.info("CStore succeed => {}", cmd);
                dcm.returnResult(Status.Success);
                break;
            case Status.CoercionOfDataElements:
                LOG.warn("CStore ID {} coercion => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.CoercionOfDataElements);
            case Status.ElementsDiscarded:
                LOG.warn("CStore ID {} elements discard => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.ElementsDiscarded);
            case Status.DataSetDoesNotMatchSOPClassWarning:
                LOG.warn("CStore ID {} data set does not match => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.DataSetDoesNotMatchSOPClassWarning);
                break;
            default:
                LOG.error("Response Error => {}, {}", TagUtils.shortToHexString(status), cmd);
        }
    }

    public void retry(IHost h, IDICOMItem dcmI) {
        this.nbRetry--;
        if (nbRetry <= 0) {
            LOG.error("Max number of retry reach, the DICOM item will be drop");
            dcmI.returnResult(Status.UnableToProcess);
            ASSOC_MANAGER.removeSession(dcmI.getAS());
        } else {
            LOG.warn("Retry to send DICOM item {} on {} in {} ms.", this.nbRetry, MAX_RETRY, (TIME_BEFORE_RETRY * (MAX_RETRY - nbRetry + 1)));
            try {
                Thread.sleep(TIME_BEFORE_RETRY * (MAX_RETRY - nbRetry + 1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Emit(h, dcmI);
        }
    }

    private void initWorker() {
        ExecutorService executorService = Executors
                .newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor();
        this.dev.setExecutor(executorService);
        this.dev.setScheduledExecutor(scheduledExecutorService);
    }
}

