package core.dicom_out.scu_services;

import core.IDICOMItem;
import core.hosts.IHost;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.IncompatibleConnectionException;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by devuser on 7/19/17.
 */
public interface IScuServices {

    Association open(IHost host) throws InterruptedException, GeneralSecurityException, IncompatibleConnectionException, IOException;

    void serve(IDICOMItem dcm, Association assoc) throws IOException, InterruptedException;

    void close() throws IOException, InterruptedException;
}
