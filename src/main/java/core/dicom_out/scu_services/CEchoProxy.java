package core.dicom_out.scu_services;


import core.hosts.Host;
import core.hosts.IHost;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.UID;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by devuser on 5/18/17.
 */
public class CEchoProxy {
    private final Device dev;
    private final ApplicationEntity fromae;
    private final Connection remote;
    private final AAssociateRQ rq = new AAssociateRQ();
    private Association as;

    public CEchoProxy(String ae, String dev) {
        this.fromae = new ApplicationEntity(ae);
        this.dev = new Device(dev);

        Connection conn = new Connection();

        this.dev.addConnection(conn);
        this.dev.addApplicationEntity(this.fromae);
        this.fromae.addConnection(conn);

        this.remote = new Connection();

        this.rq.addPresentationContext(new PresentationContext(1, UID.VerificationSOPClass,
                UID.ImplicitVRLittleEndian));

        initWorker();
    }

    public static void fortests(String name, String fromae, String device, String toae, String toip, int toport) {
        Host host = new Host(name, "description", toae, toip, toport);

        CEchoProxy cep = new CEchoProxy(fromae, device);
        cep.initWorker();

        try {
            cep.open(host);

            cep.echo();

            cep.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IncompatibleConnectionException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        fortests("TEST", "DICOM-Proxy", "DICOM-Proxy", "ECHO", "localhost", 104);
    }

    public void open(IHost h) throws IOException, InterruptedException,
            IncompatibleConnectionException, GeneralSecurityException {
        this.rq.setCalledAET(h.getAe());
        this.remote.setHostname(h.getIp());
        this.remote.setPort(h.getPort());

        Device device = new Device("DICOM-Proxy");
        Connection conn = new Connection();
        device.addConnection(conn);
        ApplicationEntity ae = new ApplicationEntity("STORESCU");
        device.addApplicationEntity(ae);
        ae.addConnection(conn);

        this.as = this.fromae.connect(remote, rq);
    }

    public void close() throws IOException, InterruptedException {
        if (this.as != null) {
            if (this.as.isReadyForDataTransfer())
                this.as.release();
            this.as.waitForSocketClose();
        }
    }

    public Attributes echo() throws IOException, InterruptedException {
        DimseRSP response = as.cecho();
        response.next();
        return response.getCommand();
    }

    private void initWorker() {
        ExecutorService executorService = Executors
                .newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor();
        this.dev.setExecutor(executorService);
        this.dev.setScheduledExecutor(scheduledExecutorService);
    }
}
