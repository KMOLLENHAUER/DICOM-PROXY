package core.dicom_out.scu_services;


import core.IDICOMItem;
import core.dicom_assoc.AssocManager;
import core.hosts.IHost;
import core.utils.ConfigurationManager;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


/**
 * Created by devuser on 5/21/17.
 */
public class CStoreProxy implements IScuServices {
    private final static Logger LOG = LoggerFactory.getLogger(CStoreProxy.class);
    private final static ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(CStoreProxy.class);
    private final static AssocManager ASSOC_MANAGER = AssocManager.getInstance();

    private final Device dev;
    private final ApplicationEntity fromae;
    private final Connection remote;
    private final AAssociateRQ rq = new AAssociateRQ();
    private IDICOMItem dcm;
    private Association as;

    public CStoreProxy(String ae, String dev, IDICOMItem dcm) {
        this.fromae = new ApplicationEntity(ae);
        this.dev = new Device(dev);
        this.dcm = dcm;

        Connection conn = new Connection();
        //conn.setBindAddress(CONFIGURATION_MANAGER.getProperty("network.hostname"));

        this.dev.addConnection(conn);
        this.dev.addApplicationEntity(this.fromae);
        this.fromae.addConnection(conn);

        this.remote = new Connection();

        this.rq.addPresentationContext(setPresentationContext(dcm));
        //enable async exchange
        if (this.rq.isAsyncOps()) {
            System.out.println("async!");
        }
        this.rq.setMaxOpsInvoked(0);
        this.rq.setMaxOpsPerformed(0);

        //this.rq.addPresentationContext(new PresentationContext(1, UID.SecondaryCaptureImageStorage, UID.ImplicitVRLittleEndian));

        //initWorker();
    }

    private PresentationContext setPresentationContext(IDICOMItem dcm) {
        return new PresentationContext(1, dcm.getDataAtt().getString(Tag.SOPClassUID),
                UID.ExplicitVRLittleEndian);
    }

    public Association open(IHost h) throws InterruptedException, GeneralSecurityException, IncompatibleConnectionException, IOException {
        this.rq.setCalledAET(h.getAe());
        this.remote.setHostname(h.getIp());
        this.remote.setPort(h.getPort());

        this.as = this.fromae.connect(remote, rq);

        return this.as;
    }

    @Override
    public void serve(IDICOMItem newdcm, Association emitterAssoc) throws IOException, InterruptedException {
        this.store(newdcm, emitterAssoc);
    }

    public void close() throws IOException, InterruptedException {
        if (this.as != null) {
            if (this.as.isReadyForDataTransfer())
                this.as.release();
            this.as.waitForSocketClose();
        }
    }

    public void store(IDICOMItem newdcm, Association emitterAssoc) throws IOException, InterruptedException {
        String cuid = newdcm.getDataAtt().getString(Tag.SOPClassUID);
        String iuid = newdcm.getDataAtt().getString(Tag.SOPInstanceUID);
        String ts = setTransfertSyntax(cuid);

        emitterAssoc.cstore(cuid, iuid, 1,
                new DataWriterAdapter(newdcm.getDataAtt()),
                ts,
                createDimseRSPHandler()
        );
    }

    private String setTransfertSyntax(String cuid) {
        Set<String> tss = as.getTransferSyntaxesFor(cuid);
        if (tss.contains(UID.ExplicitVRLittleEndian))
            return UID.ExplicitVRLittleEndian;

        return UID.ImplicitVRLittleEndian;
    }

    private void initWorker() {
        ExecutorService executorService = Executors
                .newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor();
        this.dev.setExecutor(executorService);
        this.dev.setScheduledExecutor(scheduledExecutorService);
    }

    private DimseRSPHandler createDimseRSPHandler() {

        return new DimseRSPHandler(this.as.nextMessageID()) {

            @Override
            public void onDimseRSP(Association as, Attributes cmd,
                                   Attributes data) {
                super.onDimseRSP(as, cmd, data);
                onCStoreRSP(cmd);
            }
        };
    }

    private void onCStoreRSP(Attributes cmd) {
        int status = cmd.getInt(Tag.Status, -1);
        switch (status) {
            case Status.Success:
                LOG.info("CStore ID {} succeed => {}", dcm.getAS().nextMessageID(), cmd);
                this.dcm.returnResult(Status.Success);
                break;
            case Status.CoercionOfDataElements:
                LOG.warn("CStore ID {} coercion => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.CoercionOfDataElements);
            case Status.ElementsDiscarded:
                LOG.warn("CStore ID {} elements discard => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.ElementsDiscarded);
            case Status.DataSetDoesNotMatchSOPClassWarning:
                LOG.warn("CStore ID {} data set does not match => {}", dcm.getAS().nextMessageID(), cmd);
                dcm.returnResult(Status.DataSetDoesNotMatchSOPClassWarning);
                break;
            default:
                LOG.error("{}, {}", TagUtils.shortToHexString(status), cmd);
        }
    }
}
