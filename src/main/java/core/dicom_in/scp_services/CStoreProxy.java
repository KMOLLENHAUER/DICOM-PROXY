package core.dicom_in.scp_services;


import core.DICOMItem;
import core.dicom_assoc.AssocManager;
import core.dicom_in.DicomItemCallbacks;
import core.proxy.RulesTable;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Commands;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.AbstractDicomService;
import org.dcm4che3.net.service.DicomServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by devuser on 4/25/17.
 */
public class CStoreProxy extends AbstractDicomService {
    private static final Logger LOG = LoggerFactory.getLogger(CStoreProxy.class);
    // to be able to store pending association...
    private static AssocManager ASSOC_MANAGER = AssocManager.getInstance();

    private static int instance;
    private RulesTable queue;

    public CStoreProxy() {
        super("*");
    }

    public CStoreProxy(String... sopClasses) {
        super(sopClasses);
    }

    public void setQueue(RulesTable q) {
        Objects.requireNonNull(q);
        this.queue = q;

        LOG.info("C-STORE queue added");
    }

    @Override
    protected void onDimseRQ(Association association, PresentationContext presentationContext, Dimse dimse,
                             Attributes rq, Attributes data) throws IOException {
        LOG.info("C-STORE Dimse request received.");
        if (dimse != Dimse.C_STORE_RQ) {
            throw new DicomServiceException(Status.UnrecognizedOperation);
        }
        // Create the dicom item callback
        DicomItemCallbacks cb = status -> association.tryWriteDimseRSP(presentationContext, Commands.mkCStoreRSP(rq, status));

        // if doesn't exist, create a session
        if (!ASSOC_MANAGER.existSession(association)) {
            ASSOC_MANAGER.createSession(association);
        }

        //Create a DICOMItem
        DICOMItem dI = new DICOMItem(association, presentationContext, dimse, rq, data, cb);
        //Pass the DICOMItem to the rulesTable to be check.
        queue.addInQueue(dI);
        // Tell the Association manager the nd of messages.
        ASSOC_MANAGER.addReceiveMsg(association);
        LOG.info("C-STORE request was just pushed in the rulesTable queue.");
    }

    @Override
    public void onClose(Association association) {
        // Tell the session manager that the receiver part ends.
        ASSOC_MANAGER.setReceiverFinished(association);
        LOG.info("C-STORE request handler was correctly closed.");
    }
}
