package core.dicom_in.scp_services;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.AbstractDicomService;

import java.io.IOException;

/**
 * Created by devuser on 4/25/17.
 */
public class CMoveProxy extends AbstractDicomService {
    @Override
    public void onDimseRQ(Association association, PresentationContext presentationContext, Dimse dimse, Attributes attributes, PDVInputStream pdvInputStream) throws IOException {

    }

    @Override
    protected void onDimseRQ(Association association, PresentationContext presentationContext, Dimse dimse, Attributes attributes, Attributes attributes1) throws IOException {

    }

    @Override
    public void onClose(Association association) {

    }
}
