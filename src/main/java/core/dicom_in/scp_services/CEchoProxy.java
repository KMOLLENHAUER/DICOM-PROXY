package core.dicom_in.scp_services;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Commands;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.AbstractDicomService;
import org.dcm4che3.net.service.DicomServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by devuser on 4/25/17.
 */
public class CEchoProxy extends AbstractDicomService {
    private static final Logger LOG = LoggerFactory.getLogger(CEchoProxy.class);

    public CEchoProxy() {
        super("1.2.840.10008.1.1");
    }

    public CEchoProxy(String... sopClasses) {
        super(sopClasses);
    }

    @Override
    public void onDimseRQ(Association as, PresentationContext pc, Dimse dimse, Attributes cmd, Attributes data) throws IOException {
        if (dimse != Dimse.C_ECHO_RQ) {
            throw new DicomServiceException(529);
        } else {
            as.tryWriteDimseRSP(pc, Commands.mkEchoRSP(cmd, 0));
            LOG.info("C-ECHO request was just treated.");

        }
    }

    @Override
    public void onClose(Association association) {
        LOG.info("C-Echo request handler was correctly closed.");
    }
}
