package core.dicom_in.scp_services;


import core.DICOMItem;
import core.dicom_in.DicomItemCallbacks;
import core.proxy.RulesTable;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Commands;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.AbstractDicomService;
import org.dcm4che3.net.service.DicomServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by devuser on 4/25/17.
 */
public class CFindProxy extends AbstractDicomService {
    private static final Logger LOG = LoggerFactory.getLogger(CFindProxy.class);
    private RulesTable queue;

    public CFindProxy(RulesTable q) {
        Objects.requireNonNull(q);
        this.queue = q;
    }

    @Override
    protected void onDimseRQ(Association association, PresentationContext presentationContext, Dimse dimse, Attributes rq, Attributes data) throws IOException {
        if (dimse != Dimse.C_FIND_RQ) {
            throw new DicomServiceException(Status.UnrecognizedOperation);
        }
        // Create the callback
        DicomItemCallbacks cb = status -> association.tryWriteDimseRSP(presentationContext, Commands.mkCFindRSP(rq, Status.Success));

        //Create a DICOMItem
        DICOMItem dI = new DICOMItem(association, presentationContext, dimse, rq, data, cb);
        //Pass the DICOMItem to the rulesTable to be check.
        queue.addInQueue(dI);
        LOG.info("C-FIND request was just pushed in the rulesTable queue.");

    }

    @Override
    public void onClose(Association association) {
        LOG.info("C-Find request handler was correctly closed.");
    }
}
