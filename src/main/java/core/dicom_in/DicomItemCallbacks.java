package core.dicom_in;

/**
 * Created by devuser on 7/4/17.
 */
public interface DicomItemCallbacks {
    void onResult(int status);
}
