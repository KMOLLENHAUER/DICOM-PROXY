package core.dicom_in;


import core.dicom_in.scp_services.CEchoProxy;
import core.dicom_in.scp_services.CFindProxy;
import core.dicom_in.scp_services.CMoveProxy;
import core.dicom_in.scp_services.CStoreProxy;
import core.proxy.RulesTable;
import core.utils.ConfigurationManager;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.Device;
import org.dcm4che3.net.TransferCapability;
import org.dcm4che3.net.service.DicomServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by devuser on 4/25/17.
 */
public class Receiver {
    private static final Logger LOG = LoggerFactory.getLogger(Receiver.class);

    private String aeTitle;
    private String name;
    private String commonName;
    private String hostname;
    private int port;

    private Connection con;
    private Device dev;
    private ApplicationEntity ae;
    private boolean isInitialize = false;
    private RulesTable rulesTable = RulesTable.getInstance();


    /**
     * Receiver constructor.
     *
     * @param aeTitle
     * @param name
     * @param commonName
     * @param hostName
     * @param port
     */
    private Receiver(String aeTitle, String name, String commonName, String hostName, int port) {
        Objects.requireNonNull(aeTitle);
        Objects.requireNonNull(name);
        Objects.requireNonNull(commonName);
        Objects.requireNonNull(hostName);
        Objects.requireNonNull(port);

        this.aeTitle = aeTitle;
        this.name = name;
        this.commonName = commonName;
        this.hostname = hostName;
        this.port = port;
    }

    /**
     * Create and Initialize a Receiver object from a ConfigurationManager object.
     *
     * @param pc
     * @return a Receiver object
     */
    public static Receiver receiverFactory(ConfigurationManager pc) {
        Objects.requireNonNull(pc);

        Receiver r = new Receiver(pc.getProperty("dicom.aetitle", "DICOM-PROXY"),
                pc.getProperty("dicom.name", "DICOM-Proxy"),
                pc.getProperty("dicom.commonName", "DICOM-Proxy"),
                pc.getProperty("network.hostname", "localhost"),
                Integer.parseInt(pc.getProperty("network.listeningPort", "8080")));

        r.init();
        return r;
    }

    /**
     * Create and Initialize a Receiver object from parameters.
     *
     * @param aeTitle
     * @param name
     * @param commonName
     * @param hostName
     * @param port
     * @return a Receiver object
     */
    public static Receiver receiverFactory(String aeTitle, String name, String commonName, String hostName, int port) {
        Receiver r = new Receiver(aeTitle, name, commonName, hostName, port);

        r.init();
        return r;
    }

    private void init() {
        this.ae = new ApplicationEntity(this.aeTitle);
        this.dev = new Device(this.name);
        this.con = new Connection(this.commonName, this.hostname, this.port);


        this.ae.addTransferCapability(new TransferCapability(null, "*",
                TransferCapability.Role.SCP, "*"));
        this.dev.setDimseRQHandler(createServiceRegistry());
        this.dev.addConnection(this.con);
        this.dev.addApplicationEntity(this.ae);
        this.ae.setAssociationAcceptor(true);
        this.ae.addConnection(this.con);

        this.isInitialize = true;

        LOG.info("Receiver {}, initialized.", ae.getAETitle());
    }

    public void start() throws IOException, GeneralSecurityException {
        if (!this.isInitialize)
            throw new IllegalStateException("Can't be started before initialization.");

        if (this.rulesTable == null)
            throw new IllegalStateException("No rulesTable declared.");

        ExecutorService executorService = Executors.newCachedThreadPool();
        ScheduledExecutorService scheduledExecutorService =
                Executors.newSingleThreadScheduledExecutor();
        this.dev.setScheduledExecutor(scheduledExecutorService);
        this.dev.setExecutor(executorService);
        this.dev.bindConnections();

        //Log it!
        LOG.info("Receiver started.");
    }

    public void stop() {
        //TODO
    }

    /**
     * To be able to reload the Receiver instance.
     * For example: after configuration changes or only to restart it.
     */
    public void reload(ConfigurationManager pc) {
        this.aeTitle = pc.getProperty("dicom.aetitle");
        this.name = pc.getProperty("dicom.name");
        this.commonName = pc.getProperty("dicom.commonname");
        this.hostname = pc.getProperty("network.hostname");
        this.port = Integer.parseInt(pc.getProperty("network.port"));


        stop();

        init();

        try {
            start();
        } catch (IOException | GeneralSecurityException e) {
            LOG.error("Impossible to start it after the new re-init.\n" +
                    "Configuration : {}", printCurrentConf());
        }
    }

    /**
     * Simple configuration to string!
     *
     * @return the current configuration sets.
     */
    public String printCurrentConf() {
        return "\naeTitle : " + this.aeTitle + "\nName : " + this.name + "\nCommon name : " + this.commonName +
                "\nHostname : " + this.hostname + "\nPort : " + this.port;
    }

    /**
     * Creation of the Service Registry.
     *
     * @return
     */
    private DicomServiceRegistry createServiceRegistry() {
        CStoreProxy csp = new CStoreProxy();
        csp.setQueue(this.rulesTable);

        DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();
        serviceRegistry.addDicomService(new CEchoProxy());
        serviceRegistry.addDicomService(new CFindProxy(this.rulesTable));
        serviceRegistry.addDicomService(new CMoveProxy());
        serviceRegistry.addDicomService(csp);
        return serviceRegistry;
    }

    /**
     * Return the current rules table.
     *
     * @return
     */
    public RulesTable getRulesTable() {
        return this.rulesTable;
    }

    /**
     * Set the rules table.
     *
     * @param rT
     */
    public void setRulesTable(RulesTable rT) {
        Objects.requireNonNull(rT);

        this.rulesTable = rT;
    }
}
