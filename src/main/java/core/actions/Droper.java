package core.actions;

import core.IDICOMItem;

/**
 * Created by devuser on 4/25/17.
 */
public class Droper implements IAction {

    @Override
    public IDICOMItem apply(IDICOMItem dcm) {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public String[] getParams() {
        return new String[0];
    }
}
