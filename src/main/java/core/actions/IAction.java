package core.actions;


import core.IDICOMItem;

/**
 * Created by devuser on 4/25/17.
 */
public interface IAction {
    IDICOMItem apply(IDICOMItem dcm);

    String getName();

    String getType();

    String[] getParams();
}
