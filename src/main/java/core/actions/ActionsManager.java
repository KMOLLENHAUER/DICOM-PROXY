package core.actions;

import core.triggers.TriggersManager;
import core.utils.ConfigurationManager;
import gui.JSONable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by devuser on 7/6/17.
 */
public class ActionsManager implements JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(TriggersManager.class);

    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(ActionsManager.class);

    private static ActionsManager INSTANCE;

    private final Map<String, IAction> actionMap = new HashMap<>();

    public static ActionsManager getINSTANCE() {
        // if no instance exists.
        if (INSTANCE == null) {
            // new one
            INSTANCE = new ActionsManager();
            // try to read the configuration file and load it.
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(CONFIGURATION_MANAGER.getProperty("actions.path")));
                JSONArray conf = (JSONArray) obj;

                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    String type = (String) jsonObj.get("type");
                    JSONArray params = (JSONArray) jsonObj.get("params");
                    INSTANCE.add(type, name, Arrays.stream(params.toArray()).toArray(String[]::new));
                });
            } catch (IOException e) {
                LOG.warn("Actions configuration file not found. New configuration will be create. {}", e.toString());
            } catch (ParseException e) {
                LOG.error("Impossible to parse the configuration file. New configuration will be create. {}", e.toString());
            }

            // Save current conf
            INSTANCE.saveConf();
        }

        // return Action manager
        return INSTANCE;
    }

    public static void main(String[] args) {
        ActionsManager AM = ActionsManager.getINSTANCE();

        /*AM.add("tagmodifier", "testtag1", "PATIENTUID", "NEWVALUE");
        AM.add("tagmodifier", "testtag2", "PATIENTUID", "NEWVALUE");
        AM.add("tagmodifier", "testtag3", "PATIENTUID", "NEWVALUE");
        AM.add("tagmodifier", "testtag4", "PATIENTUID", "NEWVALUE");*/

        IAction t1 = AM.getByName("testtag1");
        IAction t2 = AM.getByName("testtag2");
        IAction t3 = AM.getByName("testtag3");
        IAction t4 = AM.getByName("testtag4");

        System.out.println(t1.toString());
        System.out.println(t2.toString());
        System.out.println(t3.toString());
        System.out.println(t4.toString());

        System.out.println(AM.getAll());

        AM.saveConf();
    }

    public void add(String actiontype, String name, String params[]) throws IllegalArgumentException {

        if (actiontype.equals("tagmodifier")) {
            if (actionMap.containsKey(name)) throw new IllegalArgumentException("Action name is already taken.");
            actionMap.put(name, new TagModifier(name, params));
            return;
        }

        LOG.warn("Unknown action type : {}. Add default action instead.", actiontype);
        throw new IllegalArgumentException("Unrecognized action type.");
    }

    public IAction getByName(String name) {
        return actionMap.get(name);
    }

    public void saveConf() {
        // The conf file in JSON
        JSONArray actionsConf = new JSONArray();

        // each Action
        actionMap.forEach((k, v) -> {
            JSONObject act = new JSONObject();
            act.put("name", k);
            act.put("type", v.getType());
            JSONArray params = new JSONArray();
            for (String s : v.getParams()) {
                params.add(s);
            }
            act.put("params", params);
            actionsConf.add(act);
        });

        // Write it!
        try (FileWriter file = new FileWriter(CONFIGURATION_MANAGER.getProperty("actions.path"))) {
            file.write(actionsConf.toJSONString());
        } catch (IOException e) {
            LOG.error("Can't save the action configuration. {}", e.toString());
        }
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        JSONArray result = new JSONArray();

        actionMap.forEach((k, v) -> {
            JSONObject act = new JSONObject();
            JSONArray params = new JSONArray();
            for (String s : v.getParams()) {
                params.add(s);
            }
            act.put("name", v.getName());
            act.put("type", v.getType());
            act.put("params", params);
            result.add(act);
        });
        return result.toString();
    }

    public void deleteByName(String id) {
        actionMap.remove(id);
        LOG.info("Action : {} removed.", id);
    }
}
