package core.actions;

import core.IDICOMItem;

import java.util.Objects;

/**
 * Created by devuser on 4/25/17.
 */
public class DefaultAction implements IAction {
    /**
     * The default actions's apply is doing nothing on the DICOMItem.
     * It's use to debug or during development.
     *
     * @param dcm
     * @return
     */
    @Override
    public IDICOMItem apply(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);
        return dcm;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public String[] getParams() {
        return new String[0];
    }
}
