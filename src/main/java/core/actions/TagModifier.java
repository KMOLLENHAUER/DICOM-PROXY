package core.actions;

import core.IDICOMItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by devuser on 4/25/17.
 */
public class TagModifier implements IAction {
    private static final String type = "tagmodifier";
    private final Logger LOG = LoggerFactory.getLogger(TagModifier.class);
    private final String name;
    private final String tagname;
    private final String value;

    public TagModifier(String name, String... params) {
        if (params.length != 2) throw new IllegalArgumentException("Too few arguments.");

        this.name = name;

        this.tagname = params[0];
        this.value = params[1];
    }

    @Override
    public IDICOMItem apply(IDICOMItem dcm) {
        LOG.info("Tag {} will be modified by {}.", dcm.toString(), this.getName());
        dcm.setTagValue(tagname, value);
        LOG.info("Tag {} is modified.", dcm.toString());
        return dcm;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String[] getParams() {
        return new String[]{this.tagname, this.value};
    }
}
