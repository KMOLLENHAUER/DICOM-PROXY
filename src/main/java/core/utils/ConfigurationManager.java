package core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by devuser on 4/25/17.
 */
public class ConfigurationManager {
    private static final Map<Class, ConfigurationManager> PROXY_CONFIGURATION_MAP = new HashMap<>();
    private static final String CONF_FILE_NAME = "resources/conf.properties";
    private static Logger LOG = LoggerFactory.getLogger(ConfigurationManager.class);
    private Properties prop = new Properties();

    /**
     * Build a ConfigurationManager from a specific Path.
     *
     * @param path Path to configuration file
     */
    private ConfigurationManager(String path) {
        Objects.requireNonNull(path);

        FileNameExtensionFilter fnef = new FileNameExtensionFilter("Properties file filter", "properties");

        if (!fnef.accept(new File(path)))
            throw new IllegalArgumentException("File " + path + " is a wrong file extension.");

        try (InputStream stream = Files.newInputStream(Paths.get(path))) {
            prop.load(stream);
        } catch (Exception ioe) {
            LOG.error("Error trying to load property file {}", Paths.get(path));
            ioe.printStackTrace();
        }


    }

    /**
     * Return an instance of ConfigurationManager.
     *
     * @param clazz        class in which the proxyConfiguration will be instantiate
     * @param confFileName Path to the configuration file
     * @return
     */
    public static ConfigurationManager getInstance(Class clazz, String confFileName) {
        Objects.requireNonNull(clazz);
        Objects.requireNonNull(confFileName);

        ConfigurationManager proxProp = PROXY_CONFIGURATION_MAP.get(clazz);
        if (proxProp == null) {
            ConfigurationManager pc = new ConfigurationManager(confFileName);
            PROXY_CONFIGURATION_MAP.put(clazz, pc);
            return pc;
        }
        return proxProp;
    }

    /**
     * Return an instance of ConfigurationManager.
     *
     * @param clazz class in which the proxyConfiguration will be instantiate.
     * @return
     */
    public static ConfigurationManager getInstance(Class clazz) {
        Objects.requireNonNull(clazz);

        return getInstance(clazz, CONF_FILE_NAME);
    }

    public static void removeConfManager(Class clazz) {
        if (null == PROXY_CONFIGURATION_MAP.remove(clazz))
            LOG.warn("Configuration manager {} not found.", clazz.toString());
    }

    /**
     * Return the value of the given property or throw an IAE if the property does not exist.
     *
     * @param property The property name
     * @return
     */
    public String getProperty(String property) {
        Objects.requireNonNull(property);

        String value = prop.getProperty(property);
        if (value == null) {
            throw new IllegalArgumentException("Property not found : " + property);
        }
        return value;
    }

    /**
     * Return the value of the given property or the default value if the property does not exist.
     *
     * @param property     The property name
     * @param defaultValue Default value in case of unknown property
     * @return
     */
    public String getProperty(String property, String defaultValue) {
        Objects.requireNonNull(property);
        Objects.requireNonNull(defaultValue);

        String value = prop.getProperty(property);

        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    @Override
    public String toString() {
        return prop.toString();
    }
}
