package core.router;

import core.IDICOMItem;

/**
 * Created by devuser on 4/25/17.
 */
public interface IRoute {

    /**
     * Must be return true if at least one route is applicable.
     *
     * @param dcm a DICOMItem
     * @return true if at least one route is applicable, false otherwise
     */
    boolean isApplicable(IDICOMItem dcm);

    /**
     * Route the DICOMItem to the correct destination declare in this route.
     *
     * @param dcm a DICOMItem
     */
    void apply(IDICOMItem dcm);

    /**
     * Return a short description of the route
     *
     * @return
     */
    String toString();

    /**
     * @return
     */
    String getName();

    /**
     * @return
     */
    String getDescription();

    /**
     * @return
     */
    String getTrigger();

    /**
     * @return
     */
    String getHost();
}
