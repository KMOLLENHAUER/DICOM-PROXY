package core.router;

import core.IDICOMItem;
import core.datas.IWriteDataConnectors;
import core.datas.WriteMongoDBConnector;
import core.datas.logs.RoutingData;
import core.datas.requests.MongoWriteDataRequest;
import core.dicom_out.Emitter;
import core.hosts.IHost;
import core.triggers.ITrigger;
import gui.JSONable;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Objects;

/**
 * Created by devuser on 7/12/17.
 */
public class Route implements IRoute, JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(Route.class);
    private static final IWriteDataConnectors DB_CONNECTOR = WriteMongoDBConnector.getInstance();
    private final String name;
    private final String description;
    private final ITrigger trigger;
    private final IHost host;


    public Route(String name, String description, ITrigger trigger, IHost host) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(description);
        Objects.requireNonNull(trigger);
        Objects.requireNonNull(host);

        this.name = name;
        this.description = description;
        this.trigger = trigger;
        this.host = host;
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }

    @Override
    public boolean isApplicable(IDICOMItem dcm) {
        return trigger.isTrig(dcm);
    }

    @Override
    public void apply(IDICOMItem dcm) {
        Emitter e = new Emitter();
        e.Emit(this.host, dcm);
        DB_CONNECTOR.set(new Document().append("collection", "routing"),
                new RoutingData(new Date(), dcm.getFromAE(), host.toString(), dcm.getInstanceUID(), dcm.getStudyUID(),
                        dcm.getSeriesUID(), dcm.getPatientUID(), dcm.getImageID(),
                        dcm.getSOPClass(), "out_syntax_unsupported"),
                new MongoWriteDataRequest(),
                t -> {
                    if (t != null) {
                        LOG.error("{} {}", t.getMessage(), t.toString());
                    }
                });
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getTrigger() {
        return this.trigger.getName();
    }

    @Override
    public String getHost() {
        return this.host.getName();
    }
}
