package core.router;


import core.IDICOMItem;
import core.datas.IWriteDataConnectors;
import core.datas.WriteMongoDBConnector;
import core.datas.logs.PerfData;
import core.datas.requests.MongoWriteDataRequest;
import core.hosts.HostManager;
import core.hosts.IHost;
import core.triggers.ITrigger;
import core.triggers.TriggersManager;
import core.utils.ConfigurationManager;
import gui.JSONable;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * Created by devuser on 4/25/17.
 */
public class Router implements JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(Router.class);
    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(Router.class);
    private static final int QUEUE_SIZE = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("router.queuesize", "10"));
    private static final int NB_THREADS = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("router.nbthread", "10"));
    private static final IWriteDataConnectors DB_CONN = WriteMongoDBConnector.getInstance();
    private static final TriggersManager TRIGGER_MANAGER = TriggersManager.getINSTANCE();
    private static final HostManager HOST_MANAGER = HostManager.getINSTANCE();
    private static Router INSTANCE = null;
    private final Map<String, IRoute> routesMap = new ConcurrentHashMap<>();
    private final BlockingQueue<IDICOMItem> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    private ExecutorService process = Executors.newFixedThreadPool(NB_THREADS);

    /**
     * Router default constructor. It add the Default Route ti the routes list.
     */
    private Router() {
        // TODO route load
        //this.routes.add();
    }

    public static Router getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Router();
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(CONFIGURATION_MANAGER.getProperty("router.path")));
                JSONArray conf = (JSONArray) obj;

                //OBJECTS.
                conf.forEach((o) -> {
                    JSONObject jsonObj = (JSONObject) o;
                    String name = (String) jsonObj.get("name");
                    String description = (String) jsonObj.get("description");
                    String trigger = (String) jsonObj.get("trigger");
                    String host = (String) jsonObj.get("host");
                    INSTANCE.add(name, description, trigger, host);
                    LOG.info("Add : {}, {}, {}, {}", name, description, trigger, host);
                });
            } catch (IOException e) {
                LOG.warn("Triggers configuration file not found. New configuration will be create. {}", e.toString());
            } catch (ParseException e) {
                LOG.error("Impossible to parse the configuration file. New configuration will be create. {}", e.toString());
            }
            // Save current conf
            INSTANCE.saveConf();
        }
        return INSTANCE;
    }

    public void add(String name, String description, String trigger, String host) throws IllegalArgumentException {
        Objects.requireNonNull(name);
        Objects.requireNonNull(description);
        Objects.requireNonNull(trigger);
        Objects.requireNonNull(host);
        if (routesMap.containsKey(name)) throw new IllegalArgumentException("Route name is already taken.");
        ITrigger trig = TRIGGER_MANAGER.getByName(trigger);
        IHost hst = HOST_MANAGER.getByName(host);
        if (trig == null || hst == null) throw new IllegalArgumentException("Trigger or Host is not existing.");
        routesMap.put(name, new Route(name, description, trig, hst));
    }

    public void saveConf() {
        // The conf file in JSON
        JSONArray routerConf = new JSONArray();
        // each trigger
        routesMap.forEach((k, v) -> {
            JSONObject route = new JSONObject();
            route.put("name", k);
            route.put("description", v.getDescription());
            route.put("trigger", v.getTrigger());
            route.put("host", v.getHost());
            routerConf.add(route);
        });
        // Write it!
        try (FileWriter file = new FileWriter(CONFIGURATION_MANAGER.getProperty("router.path"))) {
            file.write(routerConf.toJSONString());
        } catch (IOException e) {
            LOG.error("Can't save the router configuration. {}", e.toString());
        }
    }

    public void deleteByName(String id) {
        routesMap.remove(id);
        LOG.info("Route : {} removed.", id);
    }

    /**
     * Route the DICOMItem given in parameter.
     *
     * @param dcm a DICOMItem
     */
    private void route(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);

        routesMap.forEach((k, v) -> {
            if (v.isApplicable(dcm)) {
                Long start = System.currentTimeMillis();
                v.apply(dcm);
                LOG.info("FIRING ROUTER RULE => " + v.getName());
                long time = System.currentTimeMillis() - start;
                DB_CONN.set(new Document().append("collection", "perf"), new PerfData("router", v.getName(), time), new MongoWriteDataRequest(),
                        t -> {
                            if (t != null) {
                                LOG.error("{} {}", t.getMessage(), t.toString());
                            }
                        });
            }
        });
    }

    /**
     * Add an DICOMItem to the routing queue.
     *
     * @param dcm a DICOMItem
     */
    public void addInQueue(IDICOMItem dcm) {
        Objects.requireNonNull(dcm);

        try {
            this.queue.put(dcm);
        } catch (InterruptedException e) {
            LOG.error("Interrupted during a try to put {} in the queue", dcm.toString());
        }
        run();

    }

    /**
     * Launch a new route worker
     */
    public void run() {
        process.submit(() -> {
            try {
                LOG.info("New Rule worker is created.");
                IDICOMItem dcm = queue.take();
                LOG.info("DICOMItem takes from the proxy rules table queue.");
                route(dcm);
            } catch (InterruptedException ie) {
                LOG.error("Rule worker was interrupted while taking DICOMItem.");
                return;
            }
            LOG.info("Rule worker finished his job.");
        });
    }

    /**
     * Try to gently stopping route workers
     */
    public void stop() {
        try {
            process.shutdown();
            //wait for 5 seconds before kill workers.
            process.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.info("Interrupted during workers shutting down.");
        } finally {
            //KILL THEM ALL!
            if (!process.isTerminated()) {
                LOG.warn("Workers will be brutally killed.");
            }
            process.shutdownNow();
            LOG.info("All workers are stopped.");
        }
    }

    /**
     * Print current configuration
     */
    public String getCurrentConf() {
        StringBuilder returnString = new StringBuilder();
        returnString.append("queue size : " + QUEUE_SIZE + "\n");
        returnString.append("loaded routes : \n");

        routesMap.forEach((k, v) -> {
            returnString.append((v.toString() + '\n'));
        });
        return returnString.toString();
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        JSONArray result = new JSONArray();
        routesMap.forEach((k, v) -> {
            JSONObject trig = new JSONObject();
            JSONArray params = new JSONArray();
            trig.put("name", v.getName());
            trig.put("description", v.getDescription());
            trig.put("trigger", v.getTrigger());
            trig.put("host", v.getHost());
            result.add(trig);
        });
        return result.toString();
    }
}
