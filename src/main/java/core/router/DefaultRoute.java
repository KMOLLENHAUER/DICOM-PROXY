package core.router;

import core.IDICOMItem;
import core.datas.IWriteDataConnectors;
import core.datas.WriteMongoDBConnector;
import core.datas.logs.RoutingData;
import core.datas.requests.MongoWriteDataRequest;
import core.dicom_out.Emitter;
import core.hosts.IHost;
import core.triggers.ITrigger;
import gui.JSONable;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * Created by devuser on 4/25/17.
 */
public class DefaultRoute implements IRoute, JSONable {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultRoute.class);
    private static final IWriteDataConnectors DB_CONNECTOR = WriteMongoDBConnector.getInstance();

    private final ArrayList<ITrigger> triggers = new ArrayList<>();
    private final List<IHost> hosts;

    public DefaultRoute(List<IHost> hosts) {
        this.hosts = hosts;
    }

    @Override
    public boolean isApplicable(IDICOMItem dcm) {
        for (ITrigger it : triggers) {
            if (!it.isTrig(dcm)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void apply(IDICOMItem dcm) {
        Emitter e = new Emitter();
        for (IHost h : hosts) {
            e.Emit(h, dcm);
            DB_CONNECTOR.set(new Document().append("collection", "routing"),
                    new RoutingData(new Date(), dcm.getFromAE(), h.toString(), dcm.getInstanceUID(), dcm.getStudyUID(),
                            dcm.getSeriesUID(), dcm.getPatientUID(), dcm.getImageID(),
                            dcm.getSOPClass(), "out_syntax_unsupported"),
                    new MongoWriteDataRequest(),
                    t -> {
                        if (t != null) {
                            LOG.error("{} {}", t.getMessage(), t.toString());
                        }
                    });
        }
    }

    @Override
    public String toString() {
        return "Trigger on : " + triggers.stream().map(ITrigger::toString).collect(joining(" or ")) +
                "Route to : " + hosts.stream().map(IHost::toString).collect(joining(" and "));
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getTrigger() {
        return null;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
