package core.router;

import core.IDICOMItem;
import core.dicom_out.Emitter;
import core.hosts.IHost;
import gui.JSONable;

import java.util.List;
import java.util.Objects;

/**
 * Created by devuser on 5/18/17.
 */
public class TestRoute implements IRoute, JSONable {
    private final List<IHost> hosts;

    public TestRoute(List<IHost> hosts) {
        Objects.requireNonNull(hosts);
        this.hosts = hosts;
    }


    @Override
    public boolean isApplicable(IDICOMItem dcm) {
        return true;
    }

    @Override
    public void apply(IDICOMItem dcm) {
        Emitter e = new Emitter();
        for (IHost h : hosts) {
            e.Emit(h, dcm);
        }

    }

    @Override
    public String toString() {
        return "Default route. Not doing interesting things...";
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getTrigger() {
        return null;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public String getAll() {
        return null;
    }
}
