import core.actions.ActionsManager;
import core.dicom_in.Receiver;
import core.hosts.HostManager;
import core.proxy.RulesTable;
import core.router.Router;
import core.triggers.TriggersManager;
import core.utils.ConfigurationManager;
import gui.WebGUI;
import io.vertx.core.Vertx;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by devuser on 5/12/17.
 */
public class TestProject {
    public static void main(String[] args) {
        //Configure the logger
        PropertyConfigurator.configure("resources/log4j.properties");

        //Obtain the logger
        Logger LOG = LoggerFactory.getLogger(TestProject.class);


        //Obtain the configuration manager
        ConfigurationManager cm = ConfigurationManager.getInstance(TestProject.class);

        // Create all triggers by invoke the manager instance
        TriggersManager.getINSTANCE();
        // Create all actions by invoke the manager instance
        ActionsManager.getINSTANCE();
        // Create all hosts by invoke the manager instance
        HostManager.getINSTANCE();


        //Create a default router.
        Router router = Router.getInstance();
        LOG.info("Default router created.");


        //Create a default proxy rules table
        RulesTable proxy = RulesTable.getInstance();
        LOG.info("Default proxy rules table created.");

        //link router to the proxy
        proxy.setRouter(router);
        LOG.info("Router declared to the proxy rules table");


        //Create the receiver
        Receiver rcv = Receiver.receiverFactory(cm);
        LOG.info("Receiver created.");

        //Add the proxy to the receiver
        rcv.setRulesTable(proxy);
        LOG.info("Proxy rules table added to the receiver.");

        //Create the web gui
        Vertx server = Vertx.vertx();


        //Start project
        try {
            // Print all conf properties
            System.out.println("Current configuration : " + cm.toString());

            //Printing the current configuration
            System.out.println("RECEIVER conf : " + rcv.printCurrentConf());

            System.out.println("\n");

            //Other interesting conf
            System.out.println("ROUTER conf : " + router.getCurrentConf());

            //Start the receiver that will start the entire application
            rcv.start();
            LOG.info("Receiver started.");

            //Finally, start the web gui
            server.deployVerticle(WebGUI.class.getName());

        } catch (IOException e1) {
            LOG.error("Error during receiver start." + e1);
        } catch (GeneralSecurityException e1) {
            LOG.error("General Security Exception during receiver start." + e1);
        }

    }
}
