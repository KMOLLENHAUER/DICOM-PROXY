package gui;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.shiro.ShiroAuth;
import io.vertx.ext.auth.shiro.ShiroAuthRealmType;


/**
 * Created by devuser on 5/31/17.
 */
public class Auth {
    private final AuthProvider provider;

    public Auth(Vertx vertx) {
        JsonObject config = new JsonObject().put("properties_path", "file:resources/test-auth.properties");

        this.provider = ShiroAuth.create(vertx, ShiroAuthRealmType.PROPERTIES, config);


    }

    public AuthProvider getProvider() {
        return this.provider;
    }

}
