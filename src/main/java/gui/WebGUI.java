package gui;

import core.actions.ActionsManager;
import core.datas.IReadDataConnectors;
import core.datas.ReadMongoDBConnector;
import core.datas.requests.DatabaseReaderCallback;
import core.datas.requests.MongoGetPerfDataRequest;
import core.datas.requests.MongoReadDataRequest;
import core.hosts.HostManager;
import core.proxy.RulesTable;
import core.triggers.TriggersManager;
import core.utils.ConfigurationManager;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by devuser on 4/28/17.
 */
public class WebGUI extends AbstractVerticle {
    //Logger
    private static final Logger LOG = LoggerFactory.getLogger(WebGUI.class);

    //Configuration manager
    private static final ConfigurationManager CONFIGURATION_MANAGER = ConfigurationManager.getInstance(WebGUI.class);

    // Router instance
    private static final core.router.Router ROUTER = core.router.Router.getInstance();

    // Proxy instance
    private static final RulesTable PROXY = RulesTable.getInstance();

    // Triggers Manager
    private static final TriggersManager TRIGGERS_MANAGER = TriggersManager.getINSTANCE();

    //Action Manager
    private static final ActionsManager ACTIONS_MANAGER = ActionsManager.getINSTANCE();

    // Host Manager
    private static final HostManager HOST_MANAGER = HostManager.getINSTANCE();

    //DataBaseConnector
    private static final IReadDataConnectors db = ReadMongoDBConnector.getInstance();


    //Set the port from the configuration manager
    private final int listeningPort = Integer.parseInt(CONFIGURATION_MANAGER.getProperty("gui.port"));

    //The server instance
    private HttpServer server;

    public static void main(String[] args) throws Exception {
        Vertx server = Vertx.vertx();

        server.deployVerticle(WebGUI.class.getName());

    }

    @Override
    public void start() {
        //Objects.requireNonNull(fut);
        Router router = Router.router(vertx);

        //Cookies, sessions and request bodies
        router.route().handler(CookieHandler.create());
        router.route().handler(BodyHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx))
                .setSessionTimeout(Integer.parseInt(CONFIGURATION_MANAGER
                        .getProperty("gui.sessiontimeout", "20000"))));

        //Creation of the Auth provider
        Auth auth = new Auth(vertx);
        AuthProvider authProvider = auth.getProvider();

        // the user session handler to store user between requests
        router.route().handler(UserSessionHandler.create(authProvider));

        // any request to the API part require login
        //TODO enable login
        /*router.route("/").handler(
                RedirectAuthHandler.create(authProvider, "/login.html"));
        router.route("/assets/*").handler(
                RedirectAuthHandler.create(authProvider, "/login.html"));
        router.route("/API/*").handler(
                RedirectAuthHandler.create(authProvider, "/login.html"));*/

        /////////////////////
        // API access part //
        /////////////////////

        // jobs
        router.get("/API/jobs/router").handler(this::getRouter);
        router.get("/API/jobs/proxy").handler(this::getProxy);

        // performances
        router.get("/API/performances").handler(this::getPerf);

        // maintenance
        router.get("/API/maintenance/triggers").handler(this::getMaintenanceTriggers);
        router.post("/API/maintenance/triggers").handler(this::addMaintenanceTrigger);
        router.delete("/API/maintenance/triggers").handler(this::deleteMaintenanceTrigger);

        router.get("/API/maintenance/actions").handler(this::getMaintenanceActions);
        router.post("/API/maintenance/actions").handler(this::addMaintenanceAction);
        router.delete("/API/maintenance/actions").handler(this::deleteMaintenanceAction);

        router.get("/API/maintenance/hosts").handler(this::getMaintenanceHosts);
        router.post("/API/maintenance/hosts").handler(this::addMaintenanceHost);
        router.delete("/API/maintenance/hosts").handler(this::deleteMaintenanceHost);

        router.get("/API/maintenance/routes").handler(this::getMaintenanceRoutes);
        router.post("/API/maintenance/routes").handler(this::addMaintenanceRoute);
        router.delete("/API/maintenance/routes").handler(this::deleteMaintenanceRoute);

        router.get("/API/maintenance/rules").handler(this::getMaintenanceRules);
        router.post("/API/maintenance/rules").handler(this::addMaintenanceRule);
        router.delete("/API/maintenance/rules").handler(this::deleteMaintenanceRule);

        // Login page
        router.route("/login").handler(FormLoginHandler.create(authProvider));

        //logout
        router.route("/logout").handler(context -> {
            context.clearUser();
            // Redirect back to the index page
            StaticHandler.create().setWebRoot("webroot");
            context.response().putHeader("location", "/").setStatusCode(302).end();
        });

        //Add the static handler to the router.
        router.route().handler(StaticHandler.create().setCachingEnabled(false).setWebRoot("webroot"));

        server = vertx.createHttpServer();

        //Give the router to the server.
        server.requestHandler(router::accept);

        //Server start to listen on port listenPort.
        server.listen(listeningPort);

        //Log it!
        LOG.info("WebGui is started and listen on port {}.", listeningPort);
    }

    /**
     * Request all the proxy entries end return it the gui through the callback
     *
     * @param routingContext
     */
    private void getRouter(RoutingContext routingContext) {
        MongoReadDataRequest req = new MongoReadDataRequest();

        // getRouter callback creation
        DatabaseReaderCallback callback = (t, result) -> routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(result);

        // request firing
        db.get(new Document().append("collection", "routing"), req, callback);
    }

    /**
     * Request all the proxy entries and returns it to the gui through the callback
     *
     * @param routingContext
     */
    private void getProxy(RoutingContext routingContext) {
        // getProxy callback creation
        DatabaseReaderCallback callback = (t, result) -> routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(result.toString());
        // request firing
        db.get(new Document().append("collection", "proxying"), new MongoReadDataRequest(), callback);
    }

    /**
     * Request perf entries and return it to the gui through the callback
     *
     * @param routingContext
     */
    private void getPerf(RoutingContext routingContext) {
        // get performances
        String nb = routingContext.request().getParam("limit");
        String type = routingContext.request().getParam("type");
        try {
            Integer nbAsInteger = Integer.valueOf(nb);
            // getPerf callback creation
            DatabaseReaderCallback callback = (t, result) -> routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(result.toString());
            // request firing
            Document perfDoc = new Document();
            perfDoc.append("collection", "perf");
            perfDoc.append("type", type);
            db.get(perfDoc, new MongoGetPerfDataRequest(), callback, nbAsInteger);
        } catch (NumberFormatException nfe) {
            LOG.error("Wrong parameter format passed in URL.");
            routingContext.response().setStatusCode(400).end();
            return;
        }
    }

    private void getMaintenanceTriggers(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
                .end(TRIGGERS_MANAGER.getAll());
    }

    private void addMaintenanceTrigger(RoutingContext routingContext) {
        JsonObject json = routingContext.getBodyAsJson();
        if (json.containsKey("name") && json.containsKey("type") && json.containsKey("params")) {
            try {
                TRIGGERS_MANAGER.add(json.getString("type"), json.getString("name"),
                        (json.getJsonArray("params").stream().toArray(String[]::new)));
            } catch (IllegalArgumentException e) {
                routingContext.response().setStatusCode(400).end("Illegal argument.");
                return;
            }
            TRIGGERS_MANAGER.saveConf();
            routingContext.response().setStatusCode(201).end();
        } else {
            routingContext.response().setStatusCode(400).putHeader("content-type", "text/plain").end("Arguments missing.");
        }
    }

    private void deleteMaintenanceTrigger(RoutingContext routingContext) {
        String id = routingContext.request().getParam("name");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            TRIGGERS_MANAGER.deleteByName(id);
            TRIGGERS_MANAGER.saveConf();
            routingContext.response().setStatusCode(204).end();
        }
    }

    private void getMaintenanceActions(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
                .end(ACTIONS_MANAGER.getAll());
    }

    private void addMaintenanceAction(RoutingContext routingContext) {
        JsonObject json = routingContext.getBodyAsJson();
        if (json.containsKey("name") && json.containsKey("type") && json.containsKey("params")) {
            try {
                ACTIONS_MANAGER.add(json.getString("type"), json.getString("name"),
                        (json.getJsonArray("params").stream().toArray(String[]::new)));
            } catch (IllegalArgumentException e) {
                routingContext.response().setStatusCode(400).end("Illegal argument.");
                return;
            }
            ACTIONS_MANAGER.saveConf();
            routingContext.response().setStatusCode(201).end();
        } else {
            routingContext.response().setStatusCode(400).putHeader("content-type", "text/plain").end("Arguments missing.");
        }
    }

    private void deleteMaintenanceAction(RoutingContext routingContext) {
        String id = routingContext.request().getParam("name");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            ACTIONS_MANAGER.deleteByName(id);
            ACTIONS_MANAGER.saveConf();
            routingContext.response().setStatusCode(204).end();
        }
    }

    private void getMaintenanceHosts(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
                .end(HOST_MANAGER.getAll());
    }

    private void addMaintenanceHost(RoutingContext routingContext) {
        JsonObject json = routingContext.getBodyAsJson();
        if (json.containsKey("name") && json.containsKey("ae") && json.containsKey("ip") && json.containsKey("port")) {
            try {
                HOST_MANAGER.add(json.getString("name"), json.getString("description"),
                        json.getString("ae"), json.getString("ip"), json.getString("port"));
            } catch (IllegalArgumentException e) {
                routingContext.response().setStatusCode(400).end("Illegal argument.");
                return;
            }
            HOST_MANAGER.saveConf();
            routingContext.response().setStatusCode(201).end();
        } else {
            routingContext.response().setStatusCode(400).putHeader("content-type", "text/plain").end("Arguments missing.");
        }
    }

    private void deleteMaintenanceHost(RoutingContext routingContext) {
        String id = routingContext.request().getParam("name");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            HOST_MANAGER.deleteByName(id);
            HOST_MANAGER.saveConf();
            routingContext.response().setStatusCode(204).end();
        }
    }

    private void getMaintenanceRoutes(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
                .end(ROUTER.getAll());
    }

    private void addMaintenanceRoute(RoutingContext routingContext) {
        JsonObject json = routingContext.getBodyAsJson();
        if (json.containsKey("name") && json.containsKey("description") && json.containsKey("trigger") && json.containsKey("host")) {
            try {
                ROUTER.add(json.getString("name"), json.getString("description"),
                        json.getString("trigger"), json.getString("host"));
            } catch (IllegalArgumentException e) {
                routingContext.response().setStatusCode(400).end("Illegal argument.");
                return;
            }
            ROUTER.saveConf();
            routingContext.response().setStatusCode(201).end();
        } else {
            routingContext.response().setStatusCode(400).putHeader("content-type", "text/plain").end("Arguments missing.");
        }
    }

    private void deleteMaintenanceRoute(RoutingContext routingContext) {
        String id = routingContext.request().getParam("name");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            ROUTER.deleteByName(id);
            ROUTER.saveConf();
            routingContext.response().setStatusCode(204).end();
        }
    }

    private void getMaintenanceRules(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
                .end(PROXY.getAll());
    }

    private void addMaintenanceRule(RoutingContext routingContext) {
        JsonObject json = routingContext.getBodyAsJson();
        if (json.containsKey("name") && json.containsKey("description") && json.containsKey("trigger") && json.containsKey("action")) {
            try {
                PROXY.add(json.getString("name"), json.getString("description"),
                        json.getString("trigger"), json.getString("action"));
            } catch (IllegalArgumentException e) {
                routingContext.response().setStatusCode(400).end("Illegal argument.");
                return;
            }
            PROXY.saveConf();
            routingContext.response().setStatusCode(201).end();
        } else {
            routingContext.response().setStatusCode(400).putHeader("content-type", "text/plain").end("Arguments missing.");
        }
    }

    private void deleteMaintenanceRule(RoutingContext routingContext) {
        String id = routingContext.request().getParam("name");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            PROXY.deleteByName(id);
            PROXY.saveConf();
            routingContext.response().setStatusCode(204).end();
        }
    }


}

