package gui;

/**
 * Created by devuser on 7/7/17.
 */
public interface JSONable {
    String toJSON();

    String getAll();
}
