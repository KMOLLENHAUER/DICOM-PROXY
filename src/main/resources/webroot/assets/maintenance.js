var url = window.location.href

$(function () {
    loadAllTriggers();
    loadAllActions();
    loadAllHosts();
    loadAllRoutes();
    loadAllRules();
});

////////////////////////////////////////////
//             TRIGGERS PART              //
////////////////////////////////////////////

function loadAllTriggers() {
    $("#triggers-table-content").children().remove();
    $.getJSON("/API/maintenance/triggers", function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>"+val.name +"</td><td>"+ val.type +"</td><td>"+ val.params+"</td><td><button type='button' class='btn btn-danger btn-sm trigger-delete' data-id='" + val.name + "'><span class='glyphicon glyphicon-minus'></span></button></td></tr>").appendTo("#triggers-table-content");
        });
        initTriggersCallbacks();
    });
}

function initTriggersCallbacks() {
    $('.trigger-delete').unbind().click(function() {
        var id = $(this).data("id");
        removetrigger(id);
    });
}

function removetrigger(id) {
        $.ajax({
            method: "DELETE",
            url: "/API/maintenance/triggers",
            data: {name: id}
        }).done(function () {
                loadAllTriggers();
            });
}

$('#triggerAdd').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);

    var name = $('#triggerAdd-name').val();
    var type = $('#triggerAdd-type').val();
    var params = $('#triggerAdd-params').val();

    if(name === '' || type === '' || params === '') {
        alert('Some fields are empty.');
    } else {

        var result = {};
        result.name = name;
        result.type = type;
        result.params = params.split(',');

        var json = JSON.stringify(result);
    // Envoi de la requête HTTP en mode asynchrone
         $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            contentType: 'application/json',
            data: json,
            success: function(html) {
                alert("Successfully added!");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Something went wrong : "+errorThrown);
            }
         });
    }
    loadAllTriggers();
});



////////////////////////////////////////////
//              ACTIONS PART              //
////////////////////////////////////////////

function loadAllActions() {
    $("#actions-table-content").children().remove();
    $.getJSON("/API/maintenance/actions", function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>"+val.name +"</td><td>"+ val.type +"</td><td>"+ val.params+"</td><td><button type='button' class='btn btn-danger btn-sm action-delete' data-id='" + val.name + "'><span class='glyphicon glyphicon-minus'></span></button></td></tr>").appendTo("#actions-table-content");
        });
        initActionsCallbacks();
    });
}

function initActionsCallbacks() {
    $('.action-delete').unbind().click(function() {
        var id = $(this).data("id");
        removeaction(id);
    });
}

function removeaction(id) {
        $.ajax({
            method: "DELETE",
            url: "/API/maintenance/actions",
            data: {name: id}
        }).done(function () {
                loadAllActions();
            });
}

$('#actionAdd').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);

    var name = $('#actionAdd-name').val();
    var type = $('#actionAdd-type').val();
    var params = $('#actionAdd-params').val();

    if(name === '' || type === '' || params === '') {
        alert('Some fields are empty.');
    } else {

        var result = {};
        result.name = name;
        result.type = type;
        result.params = params.split(',');

        var json = JSON.stringify(result);
    // Envoi de la requête HTTP en mode asynchrone
         $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            contentType: 'application/json',
            data: json,
            success: function(html) {
                alert("Successfully added!");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Something went wrong : "+errorThrown);
            }
         });
    }
    loadAllActions();
});

////////////////////////////////////////////
//               HOSTS PART               //
////////////////////////////////////////////

function loadAllHosts() {
    $("#hosts-table-content").children().remove();
    $.getJSON("/API/maintenance/hosts", function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>"+val.name +"</td><td>"+ val.description +"</td><td>"+ val.ae+"</td><td>"+ val.ip+"</td><td>"+ val.port+"</td><td><button type='button' class='btn btn-danger btn-sm host-delete' data-id='" + val.name + "'><span class='glyphicon glyphicon-minus'></span></button></td></tr>").appendTo("#hosts-table-content");
        });
        initHostsCallbacks();
    });
}

function initHostsCallbacks() {
    $('.host-delete').unbind().click(function() {
        var id = $(this).data("id");
        removehost(id);
    });
}

function removehost(id) {
        $.ajax({
            method: "DELETE",
            url: "/API/maintenance/hosts",
            data: {name: id}
        }).done(function () {
                loadAllHosts();
            });
}

$('#hostAdd').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);

    var name = $('#hostAdd-name').val();
    var description = $('#hostAdd-description').val();
    var ae = $('#hostAdd-ae').val();
    var ip = $('#hostAdd-ip').val();
    var port = $('#hostAdd-port').val();

    if(name === '' || ae === '' || ip === '' || port === '') {
        alert('Some fields are empty.');
    } else {

        var result = {};
        result.name = name;
        result.description = description;
        result.ae = ae;
        result.ip = ip;
        result.port = port;

        var json = JSON.stringify(result);
    // Envoi de la requête HTTP en mode asynchrone
         $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            contentType: 'application/json',
            data: json,
            success: function(html) {
                alert("Successfully added!");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Something went wrong : "+errorThrown);
            }
         });
    }
    loadAllHosts();
});

////////////////////////////////////////////
//               ROUTES PART               //
////////////////////////////////////////////

function loadAllRoutes() {
    $("#routes-table-content").children().remove();
    $.getJSON("/API/maintenance/routes", function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>"+val.name +"</td><td>"+ val.description +"</td><td>"+ val.trigger+"</td><td>"+ val.host+"</td><td><button type='button' class='btn btn-danger btn-sm route-delete' data-id='" + val.name + "'><span class='glyphicon glyphicon-minus'></span></button></td></tr>").appendTo("#routes-table-content");
        });
        initRoutesCallbacks();
    });
}

function initRoutesCallbacks() {
    $('.route-delete').unbind().click(function() {
        var id = $(this).data("id");
        removeroute(id);
    });
}

function removeroute(id) {
        $.ajax({
            method: "DELETE",
            url: "/API/maintenance/routes",
            data: {name: id}
        }).done(function () {
                loadAllRoutes();
            });
}

$('#routeAdd').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);

    var name = $('#routeAdd-name').val();
    var description = $('#routeAdd-description').val();
    var trigger = $('#routeAdd-trigger').val();
    var host = $('#routeAdd-host').val();

    if(name === '' || trigger === '' || host === '') {
        alert('Some fields are empty.');
    } else {

        var result = {};
        result.name = name;
        result.description = description;
        result.trigger = trigger;
        result.host = host;

        var json = JSON.stringify(result);
    // Envoi de la requête HTTP en mode asynchrone
         $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            contentType: 'application/json',
            data: json,
            success: function(html) {
                alert("Successfully added!");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Something went wrong : "+errorThrown);
            }
         });
    }
    loadAllRoutes();
});

////////////////////////////////////////////
//               RULES PART               //
////////////////////////////////////////////

function loadAllRules() {
    $("#rules-table-content").children().remove();
    $.getJSON("/API/maintenance/rules", function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>"+val.name +"</td><td>"+ val.description +"</td><td>"+ val.trigger+"</td><td>"+ val.action+"</td><td><button type='button' class='btn btn-danger btn-sm rule-delete' data-id='" + val.name + "'><span class='glyphicon glyphicon-minus'></span></button></td></tr>").appendTo("#rules-table-content");
        });
        initRulesCallbacks();
    });
}

function initRulesCallbacks() {
    $('.rule-delete').unbind().click(function() {
        var id = $(this).data("id");
        removerule(id);
    });
}

function removerule(id) {
        $.ajax({
            method: "DELETE",
            url: "/API/maintenance/rules",
            data: {name: id}
        }).done(function () {
                loadAllRules();
            });
}

$('#ruleAdd').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);

    var name = $('#ruleAdd-name').val();
    var description = $('#ruleAdd-description').val();
    var trigger = $('#ruleAdd-trigger').val();
    var action = $('#ruleAdd-action').val();

    if(name === '' || trigger === '' || action === '') {
        alert('Some fields are empty.');
    } else {

        var result = {};
        result.name = name;
        result.description = description;
        result.trigger = trigger;
        result.action = action;

        var json = JSON.stringify(result);
    // Envoi de la requête HTTP en mode asynchrone
         $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            contentType: 'application/json',
            data: json,
            success: function(html) {
                alert("Successfully added!");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Something went wrong : "+errorThrown);
            }
         });
    }
    loadAllRules();
});