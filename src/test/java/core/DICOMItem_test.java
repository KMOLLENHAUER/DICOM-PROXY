package core;

import core.dicom_in.DicomItemCallbacks;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.junit.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by devuser on 5/11/17.
 */
public class DICOMItem_test {

    @Test(expected = NullPointerException.class)
    public void test1() {
        System.out.println("core.DICOMItem TEST1");

        Association as = null;
        PresentationContext pc = null;
        Dimse dimse = null;
        Attributes at = null;
        Attributes data = null;

        DICOMItem dcm = new DICOMItem(as, pc, dimse, at, data, new DicomItemCallbacks() {
            @Override
            public void onResult(int status) {
                //nothing
            }
        });
    }

    @Test(expected = NotImplementedException.class)
    public void test2() {
        System.out.println("core.DICOMItem TEST2");

        Association as = null;
        PresentationContext pc = null;
        Dimse dimse = null;
        Attributes at = null;
        PDVInputStream data = null;

        DICOMItem dcm = new DICOMItem(as, pc, dimse, at, data);
    }
}
