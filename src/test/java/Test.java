import core.utils.ConfigurationManager;

import java.io.IOException;

/**
 * Created by devuser on 4/25/17.
 */
public class Test {
    public static void main(String[] args) throws IOException {
        ConfigurationManager pc = ConfigurationManager.getInstance(Test.class, "conf.properties");

        System.out.println(pc);

        System.out.println(pc.getProperty("dicom.aetitle", "DEFAULTVALUE"));
    }
}
