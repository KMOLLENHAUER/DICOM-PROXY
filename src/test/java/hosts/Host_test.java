package hosts;

import core.hosts.Host;
import org.junit.Test;

/**
 * Created by devuser on 5/18/17.
 */
public class Host_test {

    @Test
    public void Test1() {
        new Host("Test1", "", "TEST1AE", "localhost", 104).isReachable();
    }

    @Test
    public void Test2() {
        Host h = new Host("Test2", "", "TEST1AE", "localhost", 104);

        assert ((h.getAe().equals("TEST1AE")));
        assert ((h.getIp().equals("localhost")));
        assert ((h.getPort() == 104));
        assert (h.isReachable());
    }

    @Test
    public void Test3() {
        Host h = new Host("Test3", "", "TEST1AE", "10.10.10.10", 104);

        assert (!h.isReachable());
    }
}
