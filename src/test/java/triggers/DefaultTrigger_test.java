package triggers;

import core.DICOMItem;
import core.IDICOMItem;
import core.triggers.DefaultTrigger;
import core.triggers.ITrigger;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by devuser on 5/11/17.
 */
public class DefaultTrigger_test {

    @Test
    public void test1() {
        System.out.println("DefaultTrigger TEST1");

        ITrigger defTrig = new DefaultTrigger();
    }

    @Test(expected = NullPointerException.class)
    public void test2() {
        System.out.println("DefaultTrigger TEST2");

        ITrigger defTrig = new DefaultTrigger();

        //Cause it's the default trigger
        DICOMItem dcm = null;

        defTrig.isTrig(dcm);
    }

    @Test
    public void test3() {
        System.out.println("DefaultTrigger TEST3");

        ITrigger defTrig = new DefaultTrigger();

        //Cause it's the default trigger
        IDICOMItem dcm = new DICOMItemTestImplementation();
        assertEquals(true, defTrig.isTrig(dcm));
    }

    //For test purpose
    class DICOMItemTestImplementation implements IDICOMItem {

        @Override
        public Association getAS() {
            return null;
        }

        @Override
        public PresentationContext getPC() {
            return null;
        }

        @Override
        public Dimse getDimse() {
            return null;
        }

        @Override
        public Attributes getReq() {
            return null;
        }

        @Override
        public Attributes getDataAtt() {
            return null;
        }

        @Override
        public PDVInputStream getDataPDV() {
            return null;
        }

        @Override
        public String getFromAE() {
            return null;
        }

        @Override
        public String getInstanceUID() {
            return null;
        }

        @Override
        public String getPatientUID() {
            return null;
        }

        @Override
        public String getStudyUID() {
            return null;
        }

        @Override
        public String[] getSeriesUID() {
            return new String[0];
        }

        @Override
        public String getImageID() {
            return null;
        }

        @Override
        public String getSOPClass() {
            return null;
        }

        @Override
        public String getTagValue(String tagName) {
            return null;
        }

        @Override
        public String setTagValue(String tagName, String value) {
            return null;
        }

        @Override
        public void returnResult(int status) {
            // Nothing
        }
    }
}
