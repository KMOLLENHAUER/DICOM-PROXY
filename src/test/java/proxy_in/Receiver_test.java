package proxy_in;

import core.dicom_in.Receiver;
import core.utils.ConfigurationManager;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by devuser on 4/26/17.
 */
public class Receiver_test {

    /**
     * Test running on the default configuration.
     * Supposed to be OK.
     */
    @Test
    public void test1() {
        System.out.println("Receiver TEST1");

        Receiver r = Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class));
    }

    /**
     * Test running on the default configuration.
     * Supposed to be OK.
     */
    @Test
    @Ignore("The bind fail on the gitlab-ci runner cause it's wrongly configured.")
    public void test2() throws IOException, GeneralSecurityException, InterruptedException {
        System.out.println("Receiver TEST2");

        System.out.println("Receiver is supposed to listen to incoming connections.");
        Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class)).start();
        System.out.println("will sleep during 1s to be able to see the server start.");
        Thread.currentThread().sleep(1000);
        System.out.println("sleep ends.");
    }

    /**
     *
     */
    @Test(expected = NullPointerException.class)
    public void test3() {
        System.out.println("Receiver TEST3");

        Receiver.receiverFactory(null);
    }

    @Test
    public void test4() {
        System.out.println("Receiver TEST4");

        Receiver.receiverFactory("AETITLE", "NAME", "COMMONNAME", "1.1.1.1", 8080);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test5() throws IOException, GeneralSecurityException {
        System.out.println("Receiver TEST5");

        Receiver.receiverFactory("", "", "", "", 0).start();
    }

    /**
     * Try to open same port multiple times
     *
     * @throws IOException
     * @throws GeneralSecurityException
     */
    @Test(expected = IOException.class)
    public void test6() throws IOException, GeneralSecurityException {
        System.out.println("Receiver TEST6");

        Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class)).start();
        Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class)).start();
        Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class)).start();
        Receiver.receiverFactory(ConfigurationManager.getInstance(Receiver_test.class)).start();
    }
}
