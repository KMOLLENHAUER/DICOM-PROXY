package utils;

import core.utils.ConfigurationManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

/**
 * Created by devuser on 4/26/17.
 */
public class ConfigurationManager_test {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    /**
     * Test without error
     */
    @Test
    public void test1() throws IOException {
        System.out.println("ConfigurationManager TEST1");

        //before start remove if exist
        ConfigurationManager.removeConfManager(ConfigurationManager_test.class);

        ConfigurationManager pc = ConfigurationManager.getInstance(ConfigurationManager_test.class);
    }

    /**
     * Test with empty but correct configuration file
     */
    @Test
    public void test2() throws IOException {
        System.out.println("ConfigurationManager TEST2");

        //before start remove if exist
        ConfigurationManager.removeConfManager(ConfigurationManager_test.class);

        ConfigurationManager pc = ConfigurationManager.getInstance(ConfigurationManager_test.class,
                "emptyconf.properties");
        System.out.println(pc.toString());
        assert (pc.toString().equals("{}"));
    }

    /**
     * Test with wrong extension file
     */
    @Test(expected = IllegalArgumentException.class)
    public void test3() throws IOException {
        System.out.println("ConfigurationManager TEST3");

        //before start remove if exist
        ConfigurationManager.removeConfManager(ConfigurationManager_test.class);

        ConfigurationManager pc = ConfigurationManager.getInstance(ConfigurationManager_test.class,
                "wrongextension.propertiez");
    }

}
