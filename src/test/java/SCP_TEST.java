import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.VR;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.BasicCEchoSCP;
import org.dcm4che3.net.service.BasicCStoreSCP;
import org.dcm4che3.net.service.DicomServiceException;
import org.dcm4che3.net.service.DicomServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by devuser on 4/3/17.
 */
public class SCP_TEST {

    private static final Logger LOG = LoggerFactory.getLogger(SCP_TEST.class);
    private Connection con;
    private Device dev;
    private ApplicationEntity ae;


    public SCP_TEST() {

        this.ae = new ApplicationEntity("DICOM-PROXY");
        this.ae.addTransferCapability(new TransferCapability(null, "*",
                TransferCapability.Role.SCP, "*"));
        this.dev = new Device("DICOM-PROXY");
        this.con = new Connection("DICOM-PROXY", "127.0.0.1", 8080);
        this.dev.setDimseRQHandler(createServiceRegistry());
        this.dev.addConnection(this.con);
        this.dev.addApplicationEntity(this.ae);
        this.ae.setAssociationAcceptor(true);
        this.ae.addConnection(this.con);

        LOG.info("SCP_TEST construction end!");

    }

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        SCP_TEST main = new SCP_TEST();
        ExecutorService executorService = Executors.newCachedThreadPool();
        ScheduledExecutorService scheduledExecutorService =
                Executors.newSingleThreadScheduledExecutor();
        main.dev.setScheduledExecutor(scheduledExecutorService);
        main.dev.setExecutor(executorService);
        main.dev.bindConnections();
    }

    private DicomServiceRegistry createServiceRegistry() {
        DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();
        serviceRegistry.addDicomService(new MyCEcho());
        serviceRegistry.addDicomService(new MyCStoreSCP());
        return serviceRegistry;
    }

    private class MyCStoreSCP extends BasicCStoreSCP {
        private Logger LOG = LoggerFactory.getLogger(MyCStoreSCP.class);
        private int status;

        @Override
        public void onDimseRQ(Association as, PresentationContext pc, Dimse dimse, Attributes rq, PDVInputStream data)
                throws IOException {
            if (dimse != Dimse.C_STORE_RQ) {
                throw new DicomServiceException(529);
            } else {
                Attributes rsp = Commands.mkCStoreRSP(rq, 0);
                this.store(as, pc, rq, data, rsp);
                as.tryWriteDimseRSP(pc, rsp);
            }
        }


        public void store(Association as, PresentationContext pc, Attributes rq, PDVInputStream data, Attributes rsp) throws IOException {
            rsp.setInt(Tag.Status, VR.US, status);

            Attributes dataset = data.readDataset(pc.getTransferSyntax());

            LOG.debug("Dataset : \n {}", dataset);

            LOG.debug("Patient name : \n {}", dataset.getString(Tag.PatientName));
        }
    }

    private class MyCEcho extends BasicCEchoSCP {
        Logger LOG = LoggerFactory.getLogger(MyCEcho.class);

        @Override
        public void onDimseRQ(Association as, PresentationContext pc,
                              Dimse dimse, Attributes cmd, Attributes data) throws IOException {
            if (dimse != Dimse.C_ECHO_RQ)
                throw new DicomServiceException(Status.UnrecognizedOperation);
            as.tryWriteDimseRSP(pc, Commands.mkEchoRSP(cmd, Status.Success));
        }
    }

}
